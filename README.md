Introduction
------------
Memcached server implementation that supports (and only supports) the memcached
[text protocol](https://github.com/memcached/memcached/blob/master/doc/protocol.txt).

Quick Start
-----------
To clone this repository, build, test and start the memcached server, follow these
steps:

`git clone https://gitlab.com/pnarayanan/memcached.git`

`cd memcached`

`./gradlew build`

`java -jar ./build/libs/memcached-1.0-SNAPSHOT.jar 2>./memcached.log`

#### Command Line Arguments
The server command takes one optional argument, which is the path to the properties
file containing various configuration options, such as the host, port etc. The
config package has details of the names and default values of the various configs.
An example properties file is provided in `server.properties`.

`java -jar ./build/libs/memcached-1.0-SNAPSHOT.jar ./server.properties 2>./memcached.log`

Design
------
There are 3 main components/packages in the design: Server, Protocol and Store.

### Server
The Server is responsible for reading data from the network, writing back to the network, connection management, and request processing. For handling
client connections, it uses java NIO Selectors to follow the [Reactor pattern](https://en.wikipedia.org/wiki/Reactor_pattern). Everything runs in the
context of threads spawned by the server. Following are the main classes in the Server:
- `ConnectionAcceptor`: Runs in its own thread and uses a Selector to accept all new connections. 
Accepted connections are delegated to one of the `ConnectionHandler`s.
- `ConnectionHandler`: Handles all read and write activities on a set of connections handed over to it by the `ConnectionAcceptor`.
Each `ConnectionHandler` uses its own Selector to manage the connections assigned to it, in an event driven fashion (Reactor pattern).
The number of `ConnectionHandler`s is configurable in `ServerConfig`. For every channel/connection that it manages, it uses a
`ChannelHandler` object to interface with the `Protocol` components, and to keep track of the state of the channel. 
- `RequestResponseHandler`: Handles fully formed requests (fully decoded commands with their data, if any) by interfacing with the Store. It
blocks for more requests in the common request queue, sends dequeued requests to the store and gets the responses, and enqueues the responses for the
`ChannelHandler` associated with the request to process. The number of `RequestResponseHandler` threads is configurable in `ServerConfig`.

### Protocol
The Protocol has the intelligence to parse and interpret incoming bytes as per the memcached_text protocol specifications. It has classes and methods to
decode bytes to form commands that can be sent over to the server, and to encode responses that can be sent back to the client over the network. The
main classes are the following:
- `Decoder`: This is a stateful class, one associated with every channel, and therefore, the `ChannelHandler`. The `ChannelHandler` uses the `Decoder` 
read methods whenever new data becomes available. The `Decoder` remembers the state of parsing and continues from the last state. It uses the parsing
logic in the `Parser` to actually interpret the bytes. Every time a `Command` is fully formed, it dispatches it by enqueueing it to the request queue.
- `Parser`: The parser is a stateless class that has the actual logic to interpret bytes of a command as per the protocol specification.
- `Encoder`: The encoder is also a stateless class, and has methods that serializes the response from stores as per the protocol specification, to be sent
over the network. Unlike the `Decoder`, the `Encoder` is stateless because all bytes of a response is always available at once, and there is no need to
maintain any state.

### Store
The Store implements the actual cache. All operations to the store from callers go through the `StoreManager`. Implementations of the `Store` interface
have to support the `set` and `get` operations. Stores are instantiated via `StoreFactory` classes, and the exact `StoreFactory` to use is configurable in
`StoreConfig`. There are two concrete implementations provided:
- `SimpleLruStore`: This uses a basic synchronized `LinkedHashMap`s, that uses a Least Recently Used policy to evict entries.
- `SegmentedLruStore`: This uses a sharded implementation using a set of synchronized `LinkedHashMap`s (essentially a set of `SimpleLruStore`s) to improve
concurrency.

--

![Design diagram](img/design.png "High Level Design")

--

In short, the overall flow of request handling is this: 
- When a client establishes a connection, the `ConnectionAcceptor` accepts it and delegates the established channel to
one of the `ConnectionHandler`s in a round-robin fashion. The `ConnectionHandler` instantiates a `ChannelHandler` for the channel, and waits for any
activity to occur on it. 
- When the client sends a request, the selector within the `ConnectionHandler` returns and invokes the corresponding `ChannelHander`
with the newly read data. The `ChannelHandler` calls into the `Decoder` and the `Decoder` tries to parse the bytes from where it left off,
with the help of the `Parser` methods. For all new fully formed commands that are interpreted, the `Decoder` dispatches the command to the request queue.
- In parallel, the group of `RequestResponseHandler` are waiting on new requests to be queued in the requeus queue. When it gets a new request, it sends
it to the Store for processing. The `RequestResponseHandler` then encodes the response using the `Encoder` and puts it in the response queue associated
with the channel for the request. The notification mechanism for new responses wakes up the Selector associated with the `ConnectionHandler` for this
channel, so that it can immediately process the response.
- The `ConnectionHandler` when woken up processes any pending responses to be written out by attempting to write those responses to the channel. If not
all data could be written out, it registers the channel for WRITE interest, so that it gets notified whenever the channel becomes ready for write, so that
pending writes can continue.

Testing
-------
### Unit Testing
Unit tests test most classes and packages, and uses `Mockito` for mocking other classes, where possible. There is also an end-to-end test that launches
the server and a few clients and sends a set of requests to it. Coverage for the whole production code is around 80-90%.

### Local Testing
`telnet` could be used for testing. On Mac, one can use `nc` to connect to the server and send commands over TCP.
Note: On Mac OS, to pass `\r`, use `Ctrl-V Ctrl-M` (and press ENTER for `\n`).:
(Edit: Alternatively, pass in the -c option when starting `nc` to send CRLF as line ending (so pressing ENTER generates '\r\n' instead of just '\n')
```
$ nc localhost 11211
set key1 3 4 5^M
hello^M
STORED
set key2 3 4 5^M
world^M
STORED
get key3 key2 key1^M
VALUE key2 3 5
world
VALUE key1 3 5
hello
END
invalid command^M
ERROR
invalid command
CLIENT_ERROR Malformed command, check for trailing '\r\n'
get key2^M
VALUE key2 3 5
world
END
```

### Performance testing
For doing basic performance testing, we used Redis `memtier_benchmark` [tool](https://github.com/RedisLabs/memtier_benchmark), 
which can also be used to test memcached. Sample run that launches 16 threads, each with 50 clients:

```
$ memtier_benchmark -p 11211 -P memcache_text -t 16 -c 50 --hide-histogram
[RUN #1] Preparing benchmark client...
[RUN #1] Launching threads now...
[RUN #1 100%,  99 secs]  0 threads:     8000000 ops,   78823 (avg:   80493) ops/sec, 5.97MB/sec (avg: 6.19MB/sec), 10.15 (avg:  9.94) msec latency

16        Threads
50        Connections per thread
10000     Requests per client


ALL STATS
=========================================================================
Type         Ops/sec     Hits/sec   Misses/sec      Latency       KB/sec
-------------------------------------------------------------------------
Sets         7324.89          ---          ---      9.90800       479.88
Gets        73168.41     73168.41         0.00      9.93700      5853.72
Waits           0.00          ---          ---      0.00000          ---
Totals      80493.30     73168.41         0.00      9.93500      6333.60
```


Additional Information / Future work
------------------------------------
- Better logging and emit stats.
- Performance tuning, tune thread count, queue size, etc.
- More sophisticated implementations of the Store/Cache should be evaluated with
different eviction policies. There are cache implementations available in Guava
libraries, for example, that can be played with.
- Having separate thread pools for handling connections (`ConnectionHandler`) and
processing requests (`RequestResponseHandler`) is motivated by the philosophy of
separating business logic (that can potentially block on disk I/O say) from 
handling network events, so that the Selector threads do not block on anything else
but `select()`. This is important because a `ConnectionHandler` handles multiple
connections and we do not want a request on one channel to block work on other
channels. However, for the given use case, since all operations are in-memory,
this may not be necessary. (Originally this was motivated by making this a bit more
generic and extensible, possibly involving spillover to, say SSDs).
- Use of `ByteBuffer` at the store could probably be avoided so that a byte array
is used instead, to make reads stateless. However, in the current scheme, that
meant creating new objects, and would require a little refactoring, so this is a todo.
