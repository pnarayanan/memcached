package com.pnarayanan.memcached.store;

import com.pnarayanan.memcached.config.StoreConfig;
import org.junit.Assert;
import org.junit.Test;

import java.nio.ByteBuffer;
import java.util.Properties;
import java.util.Random;

/**
 * Tests for SegmentedLruStore.
 */
public class SegmentedLruStoreTest {
  private final SegmentedLruStore store;
  private final Random random;

  public SegmentedLruStoreTest() {
    Properties props = new Properties();
    props.setProperty("memcached.store.max.elements", "4");
    props.setProperty("memcached.store.segmented.index.num.segments", "2");
    StoreConfig storeConfig = new StoreConfig(props);
    store = new SegmentedLruStore(storeConfig);
    random = new Random();
  }

  /**
   * Since the size of the segments may not grow/shrink uniformly, we cannot
   * check for accurate number of elements
   * overall. However, there is a limit on the size of each segment, so the
   * total number of elements will never be
   * more than max elements.
   */
  @Test
  public void testSimpleSetAndGet() {
    byte[] data1 = new byte[random.nextInt(64)];
    byte[] data2 = new byte[random.nextInt(256)];
    byte[] data3 = new byte[random.nextInt(512)];
    byte[] data4 = new byte[random.nextInt(1024)];
    byte[] data5 = new byte[random.nextInt(4096)];

    store.set("key1", 1, 2, data1.length, ByteBuffer.wrap(data1));
    store.set("key2", 3, 4, data2.length, ByteBuffer.wrap(data2));
    store.set("key3", 5, 6, data3.length, ByteBuffer.wrap(data3));
    store.set("key4", 7, 8, data4.length, ByteBuffer.wrap(data4));
    store.set("key5", 9, 10, data5.length, ByteBuffer.wrap(data5));

    Assert.assertNull(store.get("never-existed"));
    Assert.assertNull(store.get("key1"));

    // Check that key is present and also that value matches. Since there are
    // 2 segments and
    // there can be upto 2 elements per segment, the 2 most recent values
    // should always be there in the cache.
    StoreTestUtil.checkValues(store, "key4", 7, data4);
    StoreTestUtil.checkValues(store, "key5", 9, data5);

    // access key3 and key1.
    store.get("key3");
    store.set("key1", 1, 2, data1.length, ByteBuffer.wrap(data1));

    StoreTestUtil.checkValues(store, "key1", 1, data1);
    StoreTestUtil.checkValues(store, "key3", 5, data3);
  }
}