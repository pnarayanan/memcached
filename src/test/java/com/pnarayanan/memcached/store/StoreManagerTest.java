package com.pnarayanan.memcached.store;

import com.pnarayanan.memcached.common.*;
import com.pnarayanan.memcached.config.StoreConfig;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Properties;
import java.util.Random;

import static org.mockito.Mockito.mock;

public class StoreManagerTest {
  private final StoreConfig storeConfig;
  private final Random random = new Random();

  public StoreManagerTest() throws Exception {
    Properties props = new Properties();
    props.setProperty("memcached.store.factory", "com.pnarayanan.memcached" +
            ".store.MockStoreFactory");
    storeConfig = new StoreConfig(props);
  }

  @Test
  public void testGetKeysSuccess() throws Exception {
    String key1 = "key1";
    byte[] data1 = new byte[5];
    random.nextBytes(data1);
    GetResponse expectedResp1 = new GetResponse(key1, 3, 4, data1.length,
            ByteBuffer.wrap(data1));

    String key2 = "key2";
    byte[] data2 = new byte[7];
    random.nextBytes(data2);
    GetResponse expectedResp2 = new GetResponse(key2, 3, 4, data2.length,
            ByteBuffer.wrap(data2));

    Store mockStore = mock(Store.class);
    Mockito.when(mockStore.get(key1)).thenReturn(expectedResp1);
    Mockito.when(mockStore.get(key2)).thenReturn(expectedResp2);

    MockStoreFactory.setStore(mockStore);
    StoreManager storeManager = new StoreManager(storeConfig);

    GetCommand mockCommand = mock(GetCommand.class);
    Mockito.when(mockCommand.getType()).thenReturn(CommandType.GET);
    Mockito.when(mockCommand.getKeys()).thenReturn(Arrays.asList(key1, key2,
            "non-existent"));

    StoreResponse resp = storeManager.handleCommand(mockCommand);

    Assert.assertNotNull(resp);
    GetCompositeResponse compResp = (GetCompositeResponse) resp;

    Assert.assertNull(compResp.getError());
    Assert.assertEquals(new HashSet<>(compResp.getGetResponseList()),
            new HashSet<>(Arrays.asList(expectedResp1, expectedResp2)));
  }

  @Test
  public void testGetKeysAllNonExistent() throws Exception {
    String key1 = "key1";
    String key2 = "key2";

    Store mockStore = mock(Store.class);
    Mockito.when(mockStore.get(key1)).thenReturn(null);
    Mockito.when(mockStore.get(key2)).thenReturn(null);

    MockStoreFactory.setStore(mockStore);
    StoreManager storeManager = new StoreManager(storeConfig);

    GetCommand mockCommand = mock(GetCommand.class);
    Mockito.when(mockCommand.getType()).thenReturn(CommandType.GET);
    Mockito.when(mockCommand.getKeys()).thenReturn(Arrays.asList(key1, key2,
            "non-existent"));

    StoreResponse resp = storeManager.handleCommand(mockCommand);

    Assert.assertNotNull(resp);
    GetCompositeResponse compResp = (GetCompositeResponse) resp;

    Assert.assertNull(compResp.getError());
    Assert.assertTrue(compResp.getGetResponseList().isEmpty());
  }

  @Test
  public void testGetKeysError() throws Exception {
    String key1 = "key1";
    byte[] data1 = new byte[5];
    random.nextBytes(data1);
    GetResponse expectedResp1 = new GetResponse(key1, 3, 4, data1.length,
            ByteBuffer.wrap(data1));

    String key2 = "key2";

    Store mockStore = mock(Store.class);
    Mockito.when(mockStore.get(key1)).thenReturn(expectedResp1);
    Mockito.when(mockStore.get(key2)).thenThrow(new IllegalStateException(
            "Mock exception"));

    MockStoreFactory.setStore(mockStore);
    StoreManager storeManager = new StoreManager(storeConfig);

    GetCommand mockCommand = mock(GetCommand.class);
    Mockito.when(mockCommand.getType()).thenReturn(CommandType.GET);
    Mockito.when(mockCommand.getKeys()).thenReturn(Arrays.asList(key1, key2,
            "non-existent"));

    StoreResponse resp = storeManager.handleCommand(mockCommand);

    Assert.assertNotNull(resp);
    GetCompositeResponse compResp = (GetCompositeResponse) resp;

    Assert.assertNotNull(compResp.getError());
    Assert.assertNull(compResp.getGetResponseList());
  }

  /**
   * Tests successful SET cases, including noreply.
   *
   * @throws Exception if there is an uncaught exception.
   */
  @Test
  public void testSetSuccess() throws Exception {
    String key1 = "key1";
    byte[] data1 = new byte[5];
    random.nextBytes(data1);
    SetResponse expectedResp = new SetResponse(null);

    Store mockStore = mock(Store.class);
    Mockito.when(mockStore.set(key1, 3, 4, data1.length,
            ByteBuffer.wrap(data1))).thenReturn(expectedResp);

    MockStoreFactory.setStore(mockStore);
    StoreManager storeManager = new StoreManager(storeConfig);

    SetCommand mockCommand = mock(SetCommand.class);
    Mockito.when(mockCommand.getType()).thenReturn(CommandType.SET);
    Mockito.when(mockCommand.getKey()).thenReturn(key1);
    Mockito.when(mockCommand.getFlags()).thenReturn(3);
    Mockito.when(mockCommand.getExptime()).thenReturn(4);
    Mockito.when(mockCommand.getLength()).thenReturn(data1.length);
    Mockito.when(mockCommand.getDataBuffer()).thenReturn(ByteBuffer.wrap(data1));

    StoreResponse resp = storeManager.handleCommand(mockCommand);

    Assert.assertNotNull(resp);
    SetResponse setResp = (SetResponse) resp;
    Assert.assertNull(setResp.getError());
  }

  @Test
  public void testSetError() throws Exception {
    String key1 = "key1";
    byte[] data1 = new byte[5];
    random.nextBytes(data1);

    Store mockStore = mock(Store.class);
    Mockito.when(mockStore.set(key1, 3, 4, data1.length,
            ByteBuffer.wrap(data1))).thenThrow(new IllegalStateException(
                    "Mock exception"));

    MockStoreFactory.setStore(mockStore);
    StoreManager storeManager = new StoreManager(storeConfig);

    SetCommand mockCommand = mock(SetCommand.class);
    Mockito.when(mockCommand.getType()).thenReturn(CommandType.SET);
    Mockito.when(mockCommand.getKey()).thenReturn(key1);
    Mockito.when(mockCommand.getFlags()).thenReturn(3);
    Mockito.when(mockCommand.getExptime()).thenReturn(4);
    Mockito.when(mockCommand.getLength()).thenReturn(data1.length);
    Mockito.when(mockCommand.getDataBuffer()).thenReturn(ByteBuffer.wrap(data1));

    StoreResponse resp = storeManager.handleCommand(mockCommand);

    Assert.assertNotNull(resp);
    SetResponse setResp = (SetResponse) resp;
    Assert.assertNotNull(setResp.getError());
  }
}

