package com.pnarayanan.memcached.store;

import org.junit.Assert;
import org.junit.Test;

import java.nio.ByteBuffer;
import java.util.Random;

public class SimpleLruStoreTest {
  private final Store store;
  private final Random random;

  public SimpleLruStoreTest() {
    store = new SimpleLruStore(3);
    random = new Random();
  }

  @Test
  public void testSimpleSetAndGet() {
    byte[] data1 = new byte[random.nextInt(64)];
    byte[] data2 = new byte[random.nextInt(256)];
    byte[] data3 = new byte[random.nextInt(512)];
    byte[] data4 = new byte[random.nextInt(1024)];
    byte[] data5 = new byte[random.nextInt(4096)];

    store.set("key1", 1, 2, data1.length, ByteBuffer.wrap(data1));
    store.set("key2", 3, 4, data2.length, ByteBuffer.wrap(data2));
    store.set("key3", 5, 6, data3.length, ByteBuffer.wrap(data3));
    store.set("key4", 7, 8, data4.length, ByteBuffer.wrap(data4));
    store.set("key5", 9, 10, data5.length, ByteBuffer.wrap(data5));

    Assert.assertNull(store.get("never-existed"));
    Assert.assertNull(store.get("key1"));
    Assert.assertNull(store.get("key2"));

    // Check that key is present and also that value matches.
    StoreTestUtil.checkValues(store, "key3", 5, data3);
    StoreTestUtil.checkValues(store, "key4", 7, data4);
    StoreTestUtil.checkValues(store, "key5", 9, data5);

    // access key3 and key1, after which key2 and key4 are the least recently
    // used.
    store.get("key3");
    store.set("key1", 1, 2, data1.length, ByteBuffer.wrap(data1));

    StoreTestUtil.checkValues(store, "key1", 1, data1);
    StoreTestUtil.checkValues(store, "key3", 5, data3);
    StoreTestUtil.checkValues(store, "key5", 9, data5);

    Assert.assertNull(store.get("key2"));
    Assert.assertNull(store.get("key4"));
  }

}