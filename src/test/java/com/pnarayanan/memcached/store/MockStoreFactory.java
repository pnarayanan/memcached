package com.pnarayanan.memcached.store;

import com.pnarayanan.memcached.config.StoreConfig;

/**
 * MockStoreFactory returns the {@link Store} that was set in it.
 */
public class MockStoreFactory implements StoreFactory {
  static Store mockStore;

  public MockStoreFactory(StoreConfig storeConfig) {
  }

  @Override
  public Store getStore() throws InstantiationException {
    return mockStore;
  }

  public static void setStore(Store store) {
    mockStore = store;
  }
}
