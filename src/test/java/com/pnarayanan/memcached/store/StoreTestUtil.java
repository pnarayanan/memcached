package com.pnarayanan.memcached.store;

import com.pnarayanan.memcached.common.GetResponse;
import org.junit.Assert;

/**
 * Helper methods for testing store.
 */
public class StoreTestUtil {
  static void checkValues(Store store, String key, int expectedFlags,
                          byte[] expectedArr) {
    GetResponse resp = store.get(key);
    Assert.assertEquals(expectedFlags, resp.getFlags());
    Assert.assertEquals(expectedArr.length, resp.getLength());
    byte[] respArr = new byte[resp.getLength()];
    System.arraycopy(resp.getValue().array(), 0, respArr, 0,
            resp.getLength());
    Assert.assertArrayEquals(expectedArr, respArr);
  }
}
