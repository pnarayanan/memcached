package com.pnarayanan.memcached.protocol;

import com.pnarayanan.memcached.common.CommandType;
import org.junit.Assert;
import org.junit.Test;

import java.nio.ByteBuffer;

/**
 * Tests {@link Parser}
 */
public class ParserTest {

  /**
   * Tests that when incomplete buffers are parsed, buffer positions get
   * reset to the beginning. When
   * complete commands are parsed, position gets advanced.
   */
  @Test
  public void testCommandLineParsing() {
    String incompleteCommand1 = "SET key 3 4 ";
    String incompleteCommand2 = "SET key 3 4 5\r";
    String incompleteCommand3 = "SET key 3 4 5\n";
    String incompleteCommand4 = "SET key 3 4 5\n54";
    String completeCommand = "SET key 3 4 5\r\n";
    String completeCommandWithMore = "SET key 3 4 5\r\nGET ";

    ParseResult result;
    ByteBuffer commandBuf = ByteBuffer.wrap(incompleteCommand1.getBytes());
    result = Parser.parseCommand(commandBuf);
    Assert.assertNull(result.error());
    Assert.assertNull(result.getCommand());
    Assert.assertEquals(0, commandBuf.position());

    commandBuf = ByteBuffer.wrap(incompleteCommand2.getBytes());
    result = Parser.parseCommand(commandBuf);
    Assert.assertNull(result.error());
    Assert.assertNull(result.getCommand());
    Assert.assertEquals(0, commandBuf.position());

    commandBuf = ByteBuffer.wrap(incompleteCommand3.getBytes());
    result = Parser.parseCommand(commandBuf);
    Assert.assertNotNull(result.error());
    Assert.assertNull(result.getCommand());
    Assert.assertEquals(14, commandBuf.position());

    commandBuf = ByteBuffer.wrap(incompleteCommand4.getBytes());
    result = Parser.parseCommand(commandBuf);
    Assert.assertNotNull(result.error());
    Assert.assertNull(result.getCommand());
    Assert.assertEquals(14, commandBuf.position());

    commandBuf = ByteBuffer.wrap(completeCommand.getBytes());
    result = Parser.parseCommand(commandBuf);
    Assert.assertNull(result.error());
    Assert.assertNotNull(result.getCommand());
    Assert.assertEquals(CommandType.SET, result.getCommand().getType());
    Assert.assertEquals(completeCommand.length(), commandBuf.position());

    // Should read only the first command, and set position at the beginning
    // of the next command which is
    // incomplete.
    commandBuf = ByteBuffer.wrap(completeCommandWithMore.getBytes());
    result = Parser.parseCommand(commandBuf);
    Assert.assertNull(result.error());
    Assert.assertNotNull(result.getCommand());
    Assert.assertEquals(CommandType.SET, result.getCommand().getType());
    Assert.assertEquals(completeCommand.length(), commandBuf.position());
    Assert.assertEquals(completeCommand.length(), commandBuf.position());
  }

  @Test
  public void testSetCommandParsing() {
    String[] goodCommands = {
            "SET key 3 4 5\r\n",
            "SET key 3 4 5 noreply\r\n"};
    String[] badCommands = {
            "SET key invalid 4 5\r\n",
            "SET key 3 4 5 invalid\r\n",
            "SET key 3\r\n",
            "SET key 3 4 5 noreply extra\r\n"};

    ParseResult result;
    ByteBuffer commandBuf;
    for (String s : goodCommands) {
      commandBuf = ByteBuffer.wrap(s.getBytes());
      result = Parser.parseCommand(commandBuf);
      Assert.assertNull(result.error());
      Assert.assertNotNull(result.getCommand());
      Assert.assertEquals(CommandType.SET, result.getCommand().getType());
      Assert.assertEquals(s.length(), commandBuf.position());
    }

    for (String s : badCommands) {
      commandBuf = ByteBuffer.wrap(s.getBytes());
      result = Parser.parseCommand(commandBuf);
      Assert.assertNotNull(result.error());
      Assert.assertNull(result.getCommand());
      Assert.assertEquals(s.length(), commandBuf.position());
    }
  }

  @Test
  public void testGetCommandParsing() {
    String[] goodCommands = {
            "GET key\r\n",
            "GET key1 key2\r\n"};
    String[] badCommands = {
            "GET\r\n",
            "GET \r\n"};

    ParseResult result;
    ByteBuffer commandBuf;
    for (String s : goodCommands) {
      commandBuf = ByteBuffer.wrap(s.getBytes());
      result = Parser.parseCommand(commandBuf);
      Assert.assertNull(result.error());
      Assert.assertNotNull(result.getCommand());
      Assert.assertEquals(CommandType.GET, result.getCommand().getType());
      Assert.assertEquals(s.length(), commandBuf.position());
    }

    for (String s : badCommands) {
      commandBuf = ByteBuffer.wrap(s.getBytes());
      result = Parser.parseCommand(commandBuf);
      Assert.assertNotNull(result.error());
      Assert.assertNull(result.getCommand());
      Assert.assertEquals(s.length(), commandBuf.position());
    }
  }

  @Test
  public void testInvalidCommandParsing() {
    String badCommand = "INVALID a b\r\n";
    ByteBuffer commandBuf = ByteBuffer.wrap(badCommand.getBytes());
    ParseResult result = Parser.parseCommand(commandBuf);
    Assert.assertNotNull(result.error());
    Assert.assertNull(result.getCommand());
    Assert.assertEquals(badCommand.length(), commandBuf.position());
  }
}