package com.pnarayanan.memcached.protocol;

import com.pnarayanan.memcached.common.Command;
import com.pnarayanan.memcached.common.CommandType;
import com.pnarayanan.memcached.common.GetCommand;
import com.pnarayanan.memcached.common.SetCommand;
import org.junit.Assert;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.nio.ByteBuffer;
import java.util.*;

public class DecoderTest {
  private final Random random = new Random();
  private Decoder decoder;
  private TestNotifier testNotifier;
  private NavigableMap<Integer, Command> commandMap;

  /**
   * For a mix of commands, this method verifies all successful scenarios.
   * It verifies that commands are processed across invocations of
   * processBytes(), that is the state handling works.
   * It verifies that in any given call, all commands for which all bytes are
   * available, are processed.
   */
  @Test
  public void testMultipleCommandsMultipleBufferRead() throws Exception {
    CommandType[][] cases = {
            {CommandType.SET},
            {CommandType.GET},
            {CommandType.SET, CommandType.SET},
            {CommandType.SET, CommandType.GET},
            {CommandType.GET, CommandType.SET},
            {CommandType.GET, CommandType.GET},
            {CommandType.GET, CommandType.SET, CommandType.GET},
            {CommandType.SET, CommandType.GET, CommandType.SET},
    };
    for (CommandType[] types : cases) {
      ByteArrayOutputStream out = new ByteArrayOutputStream();
      createCommands(out, types);
      ByteBuffer buf = ByteBuffer.wrap(out.toByteArray());
      int originalLimit = buf.limit();
      for (int i = 0; i < originalLimit; i++) {
        buf.rewind();
        testNotifier = new TestNotifier();
        decoder = new Decoder(testNotifier);
        // 1. Make the data for the commands available in two parts:[0,i) and
        // [i,n)
        // 2. Get them processed in subsequent calls.
        // 3. Ensure that Commands are formed as and when the bytes forming
        // them are available completely.

        buf.limit(i);
        decoder.processBytes(buf);
        assertExpectations(0, i);

        buf.limit(originalLimit);
        decoder.processBytes(buf);
        assertExpectations(i, originalLimit);
      }
    }
  }

  /**
   * Tests that in the case of invalid string formation, an error
   * notification happens.
   * Also verifies that subsequent commands continue to be processed.
   */
  @Test
  public void testErrorScenario() throws Exception {
    String errorInput1 = "INVALID 3 4 5\r\n";
    testNotifier = new TestNotifier();
    decoder = new Decoder(testNotifier);
    decoder.processBytes(ByteBuffer.wrap(errorInput1.getBytes()));
    Assert.assertNotNull(testNotifier.getError());

    String validInput = "GET key1 key2\r\n";
    decoder.processBytes(ByteBuffer.wrap(validInput.getBytes()));
    Assert.assertNull(testNotifier.getError());
    List<Command> lastCommands = testNotifier.getLastDispatchedCommands();
    Assert.assertEquals(1, lastCommands.size());
    Assert.assertEquals(CommandType.GET, lastCommands.get(0).getType());

    String validInputFollowedByInvalid = "GET key1 key2\r\nINVALID\r\n";
    decoder.processBytes(ByteBuffer.wrap(validInputFollowedByInvalid.getBytes()));
    Assert.assertNotNull(testNotifier.getError());
    lastCommands = testNotifier.getLastDispatchedCommands();
    Assert.assertEquals(1, lastCommands.size());
    Assert.assertEquals(CommandType.GET, lastCommands.get(0).getType());

    String invalidInputFollowedByvalid = "INVALID\r\nGET key1 key2\r\n";
    decoder.processBytes(ByteBuffer.wrap(invalidInputFollowedByvalid.getBytes()));
    Assert.assertNotNull(testNotifier.getError());
    lastCommands = testNotifier.getLastDispatchedCommands();
    Assert.assertEquals(1, lastCommands.size());
    Assert.assertEquals(CommandType.GET, lastCommands.get(0).getType());
  }

  /**
   * Asserts that all commands that were completely available between indexes
   * j and k of the input were
   * successfully formed and notified.
   *
   * @param j the start index of the input (inclusive)
   * @param k the end index of the input (exclusive)
   */
  private void assertExpectations(int j, int k) {
    Assert.assertNull(testNotifier.getError());
    List<Command> expectedCommands = new ArrayList<>(commandMap.subMap(j,
            true, k, false).values());
    Collection<Command> lastCommands = testNotifier.getLastDispatchedCommands();
    Assert.assertEquals(expectedCommands, lastCommands);
  }

  /**
   * Creates one or more commands of the type specified in the arguments, in
   * order and writes to the given {@link ByteArrayOutputStream}
   *
   * @param out  the {@link ByteArrayOutputStream} to write to.
   * @param args the {@link CommandType}s of the commands that need to be
   *             serialized into the given stream.
   */
  private void createCommands(ByteArrayOutputStream out, CommandType... args) throws Exception {
    // Use a tree map for insertion order preservation.
    commandMap = new TreeMap<>();
    for (int i = 0; i < args.length; i++) {
      CommandType type = args[i];
      if (type == CommandType.SET) {
        StringBuilder sb = new StringBuilder();
        String key = "key" + i;
        byte[] value = new byte[random.nextInt(1024) + 3];
        random.nextBytes(value);
        value[value.length - 2] = '\r';
        value[value.length - 1] = '\n';
        boolean noReply = random.nextBoolean();

        // Create the expected SetCommand, and fill its data buffer.
        SetCommand command = new SetCommand(key, 3, 4, value.length - 2,
                noReply);
        command.getDataBuffer().put(value);
        command.prepare();

        sb.append("set ");
        sb.append(key).append(" ");
        sb.append(3).append(" ");
        sb.append(4).append(" ");
        sb.append(value.length - 2);
        if (noReply) {
          sb.append(" ").append("noreply");
        }
        sb.append("\r\n");
        out.write(sb.toString().getBytes());
        out.write(value);
        commandMap.put(out.size() - 1, command);
      } else {
        StringBuilder sb = new StringBuilder();
        int numKeys = random.nextInt(8) + 1;
        List<String> keys = new ArrayList<>();
        sb.append("get");
        for (int j = 0; j < numKeys; j++) {
          String key = "key" + j;
          sb.append(" ").append(key);
          keys.add(key);
        }
        sb.append("\r\n");
        out.write(sb.toString().getBytes());
        GetCommand command = new GetCommand(keys);
        command.prepare();
        commandMap.put(out.size() - 1, command);
      }
    }
  }


  /**
   * A helper notifier used for verification.
   */
  class TestNotifier implements ParseCompletionNotifier {
    private List<Command> commandList = new ArrayList<>();
    private byte[] error;

    @Override
    public void onSuccess(Command command) {
      commandList.add(command);
    }

    @Override
    public void onError(byte[] err) {
      error = err;
    }

    List<Command> getLastDispatchedCommands() {
      List<Command> retList = commandList;
      commandList = new ArrayList<>();
      return retList;
    }

    byte[] getError() {
      byte[] retError = error;
      error = null;
      return retError;
    }
  }
}