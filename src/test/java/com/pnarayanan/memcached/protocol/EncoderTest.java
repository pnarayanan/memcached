package com.pnarayanan.memcached.protocol;

import com.pnarayanan.memcached.common.*;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import java.io.ByteArrayOutputStream;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static org.mockito.Mockito.mock;

/**
 * Tests the {@link Encoder} class.
 */
public class EncoderTest {
  private final Random random = new Random();

  /**
   * Tests encoding of SET command responses.
   */
  @Test
  public void testEncodeSetResponse() {
    SetCommand mockCommand = mock(SetCommand.class);
    Mockito.when(mockCommand.isNoreply()).thenReturn(false);
    Mockito.when(mockCommand.getType()).thenReturn(CommandType.SET);
    SetResponse mockResponse = mock(SetResponse.class);
    Mockito.when(mockResponse.getError()).thenReturn(null);
    byte[] expected = "STORED\r\n".getBytes();
    byte[] actual = Encoder.encodeResponse(mockCommand, mockResponse);
    Assert.assertArrayEquals(expected, actual);

    Mockito.when(mockCommand.isNoreply()).thenReturn(true);
    Assert.assertNull(Encoder.encodeResponse(mockCommand, mockResponse));

    Mockito.when(mockCommand.isNoreply()).thenReturn(false);
    Mockito.when(mockResponse.getError()).thenReturn("Unknown error");
    expected = Encoder.SET_GENERIC_ERROR_BYTES;
    actual = Encoder.encodeResponse(mockCommand, mockResponse);
    Assert.assertArrayEquals(expected, actual);
  }

  /**
   * Tests encoding of GET command responses.
   */
  @Test
  public void testEncodeGetResponse() throws Exception {
    GetCommand mockCommand = mock(GetCommand.class);
    Mockito.when(mockCommand.getType()).thenReturn(CommandType.GET);

    List<GetResponse> getResponseList = new ArrayList<>();
    GetResponse resp1 = mock(GetResponse.class);
    Mockito.when(resp1.getKey()).thenReturn("key1");
    Mockito.when(resp1.getFlags()).thenReturn(3);
    Mockito.when(resp1.getExptime()).thenReturn(4);
    Mockito.when(resp1.getLength()).thenReturn(5);
    byte[] value1 = new byte[5];
    random.nextBytes(value1);
    Mockito.when(resp1.getValue()).thenReturn(ByteBuffer.wrap(value1));

    GetResponse resp2 = mock(GetResponse.class);
    Mockito.when(resp2.getKey()).thenReturn("key2");
    Mockito.when(resp2.getFlags()).thenReturn(5);
    Mockito.when(resp2.getExptime()).thenReturn(7);
    Mockito.when(resp2.getLength()).thenReturn(9);
    byte[] value2 = new byte[9];
    random.nextBytes(value2);
    Mockito.when(resp2.getValue()).thenReturn(ByteBuffer.wrap(value2));

    getResponseList.add(resp1);
    getResponseList.add(resp2);

    GetCompositeResponse mockResponse = mock(GetCompositeResponse.class);
    Mockito.when(mockResponse.getError()).thenReturn(null);
    Mockito.when(mockResponse.getGetResponseList()).thenReturn(getResponseList);
    ByteArrayOutputStream out = new ByteArrayOutputStream();
    out.write("VALUE key1 3 5\r\n".getBytes());
    out.write(value1);
    out.write("\r\n".getBytes());
    out.write("VALUE key2 5 9\r\n".getBytes());
    out.write(value2);
    out.write("\r\n".getBytes());
    out.write("END\r\n".getBytes());
    byte[] expected = out.toByteArray();
    byte[] actual = Encoder.encodeResponse(mockCommand, mockResponse);
    Assert.assertArrayEquals(expected, actual);

    Mockito.when(mockResponse.getError()).thenReturn("Unknown error");
    expected = Encoder.GET_GENERIC_ERROR_BYTES;
    actual = Encoder.encodeResponse(mockCommand, mockResponse);
    Assert.assertArrayEquals(expected, actual);
  }

  /**
   * Tests encoding of error responses.
   */
  @Test
  public void testEncodeErrorResponse() {
    GetCommand mockCommand = mock(GetCommand.class);
    Mockito.when(mockCommand.getType()).thenReturn(null);
    byte[] expected = Encoder.GENERIC_CLIENT_ERROR_BYTES;
    byte[] actual = Encoder.encodeResponse(mockCommand,
            mock(StoreResponse.class));
    Assert.assertArrayEquals(expected, actual);
  }
}