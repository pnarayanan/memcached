package com.pnarayanan.memcached.server;

import com.pnarayanan.memcached.common.Command;
import com.pnarayanan.memcached.common.CommandType;
import com.pnarayanan.memcached.common.GetCommand;
import com.pnarayanan.memcached.common.SetCommand;
import org.junit.Test;

import java.io.*;
import java.net.Socket;
import java.util.*;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

/**
 * Tests the memcached server end-to-end.
 */
public class EndToEndTest {
  private final Random random = new Random();

  private static final String host = "localhost";
  private static final int port = 11211;

  /**
   * Sets up the server and clients, and executes a mix of SET and GET commands.
   */
  @Test
  public void endToEndTest() throws Exception {
    AtomicBoolean keepRunning = new AtomicBoolean(true);
    AtomicReference<Exception> exceptionReference = new AtomicReference<>();
    final int numClients = 10;
    CountDownLatch clientsFinishLatch = new CountDownLatch(numClients);
    CountDownLatch serverStartedLatch = new CountDownLatch(1);
    Thread mainThread = new Thread(() -> {
      try {
        Server server = new Server();
        server.start(new Properties(), keepRunning);
        serverStartedLatch.countDown();
        server.awaitShutdown();
      } catch (Exception e) {
        exceptionReference.set(e);
        serverStartedLatch.countDown();
      }
    });
    mainThread.start();
    serverStartedLatch.await();
    if (exceptionReference.get() != null) {
      throw exceptionReference.get();
    }
    for (int i = 0; i < numClients; i++) {
      Thread t = new Thread(new ClientThread(clientsFinishLatch,
              exceptionReference));
      t.start();
    }
    clientsFinishLatch.await();
    keepRunning.set(false);
    if (exceptionReference.get() != null) {
      throw exceptionReference.get();
    }
  }

  /**
   * Represents a client thread/connection.
   */
  class ClientThread implements Runnable {
    private final CountDownLatch latch;
    private final AtomicReference<Exception> exceptionReference;

    ClientThread(CountDownLatch latch,
                 AtomicReference<Exception> exceptionReference) {
      this.latch = latch;
      this.exceptionReference = exceptionReference;
    }

    @Override
    public void run() {
      try (Socket socket = new Socket(host, port)) {
        OutputStream out = socket.getOutputStream();
        InputStream in = socket.getInputStream();
        BufferedReader reader = new BufferedReader(new InputStreamReader(in));
        CommandType[][] types = {
                {CommandType.SET},
                {CommandType.GET},
                {CommandType.SET, CommandType.SET},
                {CommandType.SET, CommandType.GET},
                {CommandType.GET, CommandType.SET},
                {CommandType.GET, CommandType.GET},
                {CommandType.GET, CommandType.SET, CommandType.GET},
                {CommandType.SET, CommandType.GET, CommandType.SET},
        };

        for (int i = 0; i < 10; i++) {
          HashMap<byte[], Command> map =
                  createCommands(types[random.nextInt(types.length)]);
          for (Map.Entry<byte[], Command> entry : map.entrySet()) {
            out.write(entry.getKey());
            if (entry.getValue().getType() == CommandType.SET) {
              SetCommand command = (SetCommand) entry.getValue();
              if (!command.isNoreply()) {
                String line = reader.readLine();
                if (!line.equals("STORED")) {
                  throw new IllegalStateException("Unexpected response from " +
                          "server: " + line);
                }
              }
            } else {
              // this is a primitive way of checking, and can be improved.
              // Keep reading as many lines in the response
              // until we get what we are looking for. If there is an error,
              // however, this may block. @todo: Improve
              while (true) {
                String line = reader.readLine();
                if (line.equals("END")) {
                  break;
                } else if (line.startsWith("ERROR") ||
                        line.startsWith("CLIENT_ERROR") ||
                        line.startsWith("SERVER_ERROR")) {
                  throw new IllegalStateException("Unexpected response from " +
                          "server: " + line);
                }
              }
            }
          }
        }
      } catch (Exception e) {
        exceptionReference.set(e);
      } finally {
        latch.countDown();
      }
    }

    /**
     * Creates commands based on the argument types.
     *
     * @param args a list of {@link CommandType}. A {@link Command} and
     *             request bytes will be generated for each arg.
     * @return a map from request bytes to the {@link Command} for each
     * command created.
     */
    private HashMap<byte[], Command> createCommands(CommandType... args) throws Exception {
      // Use a tree map for insertion order preservation.
      HashMap<byte[], Command> commandAndExpectedResponse = new HashMap<>();
      for (int i = 0; i < args.length; i++) {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        CommandType type = args[i];
        if (type == CommandType.SET) {
          StringBuilder sb = new StringBuilder();
          String key = "key" + i;
          byte[] value = new byte[random.nextInt(1024) + 3];
          random.nextBytes(value);
          value[value.length - 2] = '\r';
          value[value.length - 1] = '\n';
          boolean noReply = random.nextBoolean();

          // Create the expected SetCommand, and fill its data buffer.
          SetCommand command = new SetCommand(key, 3, 4, value.length - 2,
                  noReply);
          command.getDataBuffer().put(value);
          command.prepare();

          sb.append("set ");
          sb.append(key).append(" ");
          sb.append(3).append(" ");
          sb.append(4).append(" ");
          sb.append(value.length - 2);
          if (noReply) {
            sb.append(" ").append("noreply");
          }
          sb.append("\r\n");
          out.write(sb.toString().getBytes());
          out.write(value);

          commandAndExpectedResponse.put(out.toByteArray(), command);
        } else {
          StringBuilder sb = new StringBuilder();
          int numKeys = random.nextInt(8) + 1;
          List<String> keys = new ArrayList<>();
          sb.append("get");
          for (int j = 0; j < numKeys; j++) {
            String key = "key" + j;
            sb.append(" ").append(key);
            keys.add(key);
          }
          sb.append("\r\n");
          out.write(sb.toString().getBytes());
          GetCommand command = new GetCommand(keys);
          command.prepare();
          commandAndExpectedResponse.put(out.toByteArray(), command);
        }
      }
      return commandAndExpectedResponse;
    }
  }
}