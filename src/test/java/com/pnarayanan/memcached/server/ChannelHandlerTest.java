package com.pnarayanan.memcached.server;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.mockito.stubbing.Answer;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;

import static org.mockito.Mockito.mock;

/**
 * Tests for {@link ChannelHandler}
 */
public class ChannelHandlerTest {
  /**
   * Tests basics like instantiation and closing.
   */
  @Test
  public void testInstantiationAndShutdown() {
    SocketChannel socketChannel = mock(SocketChannel.class);
    RequestResponseChannel requestResponseChannel =
            mock(RequestResponseChannel.class);
    ConnectionHandler connectionHandler = mock(ConnectionHandler.class);
    ChannelHandler channelHandler = new ChannelHandler(socketChannel,
            requestResponseChannel, connectionHandler);
    Assert.assertEquals(socketChannel, channelHandler.getChannel());
    Assert.assertEquals(connectionHandler,
            channelHandler.getConnectionHandler());
    Assert.assertTrue(channelHandler.isActive());
    channelHandler.close();
    Assert.assertFalse(channelHandler.isActive());
  }

  /**
   * Tests reads. Verifies that onRead() returns true only when there is no
   * more reads
   * possible.
   */
  @Test
  public void testOnRead() throws Exception {
    SocketChannel mockSocketChannel = mock(SocketChannel.class);
    RequestResponseChannel requestResponseChannel =
            mock(RequestResponseChannel.class);
    ConnectionHandler connectionHandler = mock(ConnectionHandler.class);
    ChannelHandler channelHandler = new ChannelHandler(mockSocketChannel,
            requestResponseChannel, connectionHandler);

    Mockito.when(mockSocketChannel.read(Mockito.any(ByteBuffer.class))).thenReturn(10).thenReturn(0);
    Assert.assertFalse(channelHandler.onRead());
    Mockito.when(mockSocketChannel.read(Mockito.any(ByteBuffer.class))).thenReturn(10).thenReturn(-1);
    Assert.assertTrue(channelHandler.onRead());

    // Subsequent reads also should return true.
    Assert.assertTrue(channelHandler.onRead());

    channelHandler = new ChannelHandler(mockSocketChannel,
            requestResponseChannel, connectionHandler);
    Mockito.when(mockSocketChannel.read(Mockito.any(ByteBuffer.class))).thenThrow(new IOException());
    Assert.assertTrue(channelHandler.onRead());
  }

  /**
   * Tests writes. Verifies that queued writes are all attempted to be
   * written out, and as much as possible.
   * That is, write pauses only if the channel cannot take any more writes at
   * any point.
   * .
   */
  @Test
  public void testWrite() throws Exception {
    SocketChannel mockSocketChannel = mock(SocketChannel.class);
    RequestResponseChannel requestResponseChannel =
            mock(RequestResponseChannel.class);
    ConnectionHandler connectionHandler = mock(ConnectionHandler.class);
    ChannelHandler channelHandler = new ChannelHandler(mockSocketChannel,
            requestResponseChannel, connectionHandler);

    // Set up the channel so that it take writes partially and then returns 0
    // indicating no more
    // writes possible at this point.
    Mockito.when(mockSocketChannel.write(Mockito.any(ByteBuffer.class))).thenAnswer((Answer<Integer>) invocation -> {
      ByteBuffer buf = invocation.getArgumentAt(0, ByteBuffer.class);
      if (buf.hasRemaining()) {
        buf.position(buf.position() + 1);
        return 1;
      } else {
        return 0;
      }
    }).thenReturn(0);
    byte[] a = new byte[5];

    // False indicates not all writes have gone through yet.
    Assert.assertFalse(channelHandler.write(a));

    // Set up the channel to write as many bytes as needed.
    Mockito.when(mockSocketChannel.write(Mockito.any(ByteBuffer.class))).thenAnswer((Answer<Integer>) invocation -> {
      ByteBuffer buf = invocation.getArgumentAt(0, ByteBuffer.class);
      if (buf.hasRemaining()) {
        buf.position(buf.position() + 1);
        return 1;
      } else {
        return 0;
      }
    });
    // True indicates all pending writes have gone through, including the
    // argument.
    Assert.assertTrue(channelHandler.write(a));
  }
}