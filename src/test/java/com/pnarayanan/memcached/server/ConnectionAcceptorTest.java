package com.pnarayanan.memcached.server;

import org.junit.Test;

import java.net.Socket;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import static org.mockito.Mockito.*;

/**
 * Tests for {@link ConnectionAcceptor}
 */
public class ConnectionAcceptorTest {
  private static final String host = "localhost";
  private static final int port = 11311;

  /**
   * Test basic connection acceptance and delegation to
   * {@link ConnectionHandler}s.
   */
  @Test
  public void testConnectionAcceptor() throws Exception {
    List<ConnectionHandler> connHandlerList = new ArrayList<>();
    ConnectionHandler mockHandler1 = mock(ConnectionHandler.class);
    ConnectionHandler mockHandler2 = mock(ConnectionHandler.class);
    connHandlerList.add(mockHandler1);
    connHandlerList.add(mockHandler2);
    AtomicBoolean keepRunning = new AtomicBoolean(true);

    // Start ConnectionAcceptor thread
    ConnectionAcceptor connectionAcceptor = new ConnectionAcceptor(host, port
            , connHandlerList, keepRunning);
    Thread t = new Thread(connectionAcceptor);
    t.start();

    // Start client connection
    Socket socket1 = new Socket(host, port);
    Socket socket2 = new Socket(host, port);
    while (connectionAcceptor.acceptedConnections() < 2) {
      Thread.sleep(1000);
    }
    keepRunning.set(false);
    socket1.close();
    socket2.close();
    connectionAcceptor.awaitShutdown();

    // Verify that the connections were accepted and delegated evenly to the
    // handlers in the list.
    verify(mockHandler1, times(1)).add(any(SocketChannel.class));
    verify(mockHandler2, times(1)).add(any(SocketChannel.class));
  }
}