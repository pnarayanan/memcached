package com.pnarayanan.memcached.server;

import com.pnarayanan.memcached.common.Command;
import com.pnarayanan.memcached.common.StoreResponse;
import com.pnarayanan.memcached.store.StoreManager;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.concurrent.atomic.AtomicBoolean;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Tests {@link RequestResponseHandler}
 */
public class RequestResponseHandlerTest {

  /**
   * Basic tests to ensure that requests are dequeued and responses are
   * enqueued.
   */
  @Test
  public void testRequestResponseHandling() throws InterruptedException {
    Request request = mock(Request.class);
    Command command = mock(Command.class);
    when(request.getCommand()).thenReturn(command);
    RequestResponseChannel requestResponseChannel =
            mock(RequestResponseChannel.class);
    when(requestResponseChannel.dequeueRequest()).thenReturn(request).thenThrow(new InterruptedException(" "));
    StoreManager storeManager = mock(StoreManager.class);
    StoreResponse response = mock(StoreResponse.class);
    when(storeManager.handleCommand(Mockito.any(Command.class))).thenReturn(response);

    RequestResponseHandler requestResponseHandler =
            new RequestResponseHandler(requestResponseChannel, storeManager,
                    new AtomicBoolean(true));
    requestResponseHandler.run();
    Mockito.verify(requestResponseChannel, Mockito.times(1)).enqueueResponse(Mockito.any(byte[].class),
            Mockito.any(Request.class));
  }
}