package com.pnarayanan.memcached.common;

/**
 * Represent a response from the store.
 */
public interface StoreResponse {
  /**
   * @return the error associated with this response, if there is one.
   */
  String getError();
}
