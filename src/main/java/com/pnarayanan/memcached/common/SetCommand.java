package com.pnarayanan.memcached.common;

import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * SET Command implementation.
 */
public class SetCommand implements Command {
  private final String key;
  private final int flags;
  private final int exptime;
  private final int length;
  private final boolean noreply;
  private final ByteBuffer datablock;

  /**
   * Instantiate a SetCommand object with the given list of keys as arguments.
   *
   * @param key     the key
   * @param flags   the flags
   * @param exptime the expirty time.
   * @param length  the length of the data block.
   * @param noreply whether a response should be omitted.
   */
  public SetCommand(String key, int flags, int exptime, int length,
                    boolean noreply) {
    this.key = key;
    this.flags = flags;
    this.exptime = exptime;
    this.length = length;
    this.noreply = noreply;
    this.datablock = ByteBuffer.allocate(length + 2);
  }

  /**
   * @return the datablock associated with this command.
   */
  @Override
  public ByteBuffer getDataBuffer() {
    return datablock;
  }

  /**
   * @return true if the data block has been completely read, false otherwise.
   */
  @Override
  public boolean isReadyToSend() {
    return !datablock.hasRemaining();
  }

  /**
   * Prepares the data block for processing by trimming off the trailing
   * carriage return and new line characters.
   */
  @Override
  public void prepare() {
    // Remove the trailing '\r' and '\n'
    datablock.position(length);
    datablock.flip();
  }

  /**
   * @return returns {@link CommandType#SET}
   */
  @Override
  public CommandType getType() {
    return CommandType.SET;
  }

  /**
   * @return the key associated with this command.
   */
  public String getKey() {
    return key;
  }

  /**
   * @return the flags associated with this command.
   */
  public int getFlags() {
    return flags;
  }

  /**
   * @return the exptime associated with this command.
   */
  public int getExptime() {
    return exptime;
  }

  /**
   * @return the length of the data blocks for this command (excluding
   * trailing '\r' and '\n')
   */
  public int getLength() {
    return length;
  }

  /**
   * @return true if noreply is set, false otherwise.
   */
  public boolean isNoreply() {
    return noreply;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    SetCommand that = (SetCommand) o;
    return that.key.equals(key) &&
            that.flags == flags &&
            that.exptime == exptime &&
            that.length == length &&
            that.noreply == noreply &&
            that.datablock.equals(datablock);
  }

  @Override
  public int hashCode() {
    return Objects.hash(key, flags, exptime, length, noreply, datablock);
  }

  @Override
  public String toString() {
    List<Object> items = Arrays.asList("set", key, flags, exptime, length,
            noreply, datablock.toString());
    return items.stream().map(Object::toString).collect(Collectors.joining(" "
    ));
  }
}
