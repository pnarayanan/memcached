package com.pnarayanan.memcached.common;

/**
 * The valid command types understood by this server.
 */
public enum CommandType {
  SET, // Represents a SET command.
  GET  // Represents a GET command.
}

