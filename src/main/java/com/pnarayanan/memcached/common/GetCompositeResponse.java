package com.pnarayanan.memcached.common;

import java.util.List;

/**
 * Composite response composed of multiple {@link GetResponse}s associated
 * with a multi-get.
 */
public class GetCompositeResponse implements StoreResponse {
  private final List<GetResponse> getResponses;
  private final String error;

  /**
   * Instantiate a GetCompositeResponse
   *
   * @param getResponses the list of {@link GetResponse}
   * @param error        the error if there is one.
   */
  public GetCompositeResponse(List<GetResponse> getResponses, String error) {
    this.error = error;
    this.getResponses = getResponses;
  }

  /**
   * @return the list of {@link GetResponse}
   */
  public List<GetResponse> getGetResponseList() {
    return getResponses;
  }

  /**
   * @return the error associated with this response, if there is one.
   */
  @Override
  public String getError() {
    return error;
  }
}

