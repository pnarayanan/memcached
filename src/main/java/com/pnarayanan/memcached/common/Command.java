package com.pnarayanan.memcached.common;

import java.nio.ByteBuffer;

/**
 * Represents a command and all common attributes associated with it.
 */
public interface Command {
  /**
   * The type of the command.
   *
   * @return the {@link CommandType} of the command.
   */
  CommandType getType();

  /**
   * Whether this command is fully formed and is ready to be sent for
   * processing.
   *
   * @return true if it is ready; false otherwise.
   */
  boolean isReadyToSend();

  /**
   * Prepare this command for processing.
   */
  void prepare();

  /**
   * The data block associated with this command, if any.
   *
   * @return a {@link ByteBuffer} containing the data block.
   */
  ByteBuffer getDataBuffer();
}
