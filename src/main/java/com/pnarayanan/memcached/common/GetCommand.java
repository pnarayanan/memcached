package com.pnarayanan.memcached.common;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * GET Command implementation.
 */
public class GetCommand implements Command {
  private final List<String> keys;

  /**
   * Instantiate a GetCommand object with the given list of keys as arguments.
   *
   * @param keys the keys to fetch for the GET operation.
   */
  public GetCommand(List<String> keys) {
    this.keys = keys;
  }

  /**
   * There is no data block for the GET command.
   *
   * @return Always returns null.
   */
  @Override
  public ByteBuffer getDataBuffer() {
    return null;
  }

  /**
   * Get Command is always ready to send once instantiated.
   *
   * @return Always returns true.
   */
  @Override
  public boolean isReadyToSend() {
    return true;
  }

  /**
   * Get Command does not require any preparation.
   */
  @Override
  public void prepare() {
    // Nothing to do.
  }

  /**
   * @return returns {@link CommandType#GET}
   */
  @Override
  public CommandType getType() {
    return CommandType.GET;
  }

  /**
   * @return the list of keys that are the arguments for this command.
   */
  public List<String> getKeys() {
    return keys;
  }

  @Override
  public String toString() {
    List<Object> items = new ArrayList<>();
    items.add("get");
    items.addAll(keys);
    return items.stream().map(Object::toString).collect(Collectors.joining(" "
    ));
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    GetCommand that = (GetCommand) o;
    return keys.equals(that.keys);
  }

  @Override
  public int hashCode() {
    return Objects.hash(keys);
  }
}
