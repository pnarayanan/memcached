package com.pnarayanan.memcached.common;

import java.nio.ByteBuffer;

/**
 * Response associated from a store {@link GetCommand}
 */
public class GetResponse implements StoreResponse {
  private final String key;
  private final int flags;
  private final int exptime;
  private final int length;
  private final ByteBuffer value;
  private final String error;

  /**
   * Instantiate a GetResponse
   *
   * @param key     the key associated with the Get
   * @param flags   the flags associated with the key
   * @param exptime expiry time associated with the key
   * @param length  length of the data
   * @param value   {@link ByteBuffer} associated with this key.
   */
  public GetResponse(String key, int flags, int exptime, int length,
                     ByteBuffer value) {
    this.error = null;
    this.key = key;
    this.flags = flags;
    this.exptime = exptime;
    this.length = length;
    this.value = value;
  }

  /**
   * Instantiate an error response
   *
   * @param error the error associated with this response.
   */
  public GetResponse(String error) {
    this.error = error;
    this.key = null;
    this.flags = 0;
    this.exptime = 0;
    this.length = 0;
    this.value = null;
  }

  /**
   * @return the key
   */
  public String getKey() {
    return key;
  }

  /**
   * @return the flags
   */
  public int getFlags() {
    return flags;
  }

  /**
   * @return the expiry time
   */
  public int getExptime() {
    return exptime;
  }

  /**
   * @return the length of the data
   */
  public int getLength() {
    return length;
  }

  /**
   * @return the data
   */
  public ByteBuffer getValue() {
    return value;
  }

  /**
   * @return the error associated with this response, if there is one.
   */
  @Override
  public String getError() {
    return error;
  }
}

