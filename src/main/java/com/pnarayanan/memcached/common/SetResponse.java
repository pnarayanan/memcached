package com.pnarayanan.memcached.common;

/**
 * Response associated from a store {@link SetCommand}
 */
public class SetResponse implements StoreResponse {
  private final String error;

  /**
   * Instantiate a {@link SetResponse}
   *
   * @param error the error associated with this response, if there is one.
   */
  public SetResponse(String error) {
    this.error = error;
  }

  /**
   * @return the error associated with this response, if there is one.
   */
  @Override
  public String getError() {
    return error;
  }
}
