package com.pnarayanan.memcached.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Constructor;
import java.nio.ByteBuffer;
import java.util.Properties;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * General utility functions.
 */
public class Utils {
  /**
   * Attempts to read a line. More specifically, attempts to read until a
   * carriage return followed by a new line
   * is seen in the given {@link ByteBuffer}.
   *
   * @param buf the {@link ByteBuffer} from which to read.
   * @return returns the line that is read excluding the trailing carriage
   * return ('\r') and new line ('\n'), or null
   * if such a pattern was not encountered.
   * @throws IllegalStateException if a '\n' was seen without a preceding '\r'.
   */
  public static String maybeReadLine(ByteBuffer buf) {
    StringBuilder sb = new StringBuilder();
    int prev = 0;
    int cur = 0;
    while (buf.hasRemaining() && cur != '\n') {
      prev = cur;
      cur = buf.get();
      if (cur != '\r' && cur != '\n') {
        sb.append((char) cur);
      }
    }
    if (cur != '\n') {
      return null;
    }
    if (prev != '\r') {
      throw new IllegalArgumentException("Unexpected EOL");
    }
    return sb.toString();
  }

  /**
   * Instantiate a class instance from a given className with variable number
   * of args
   * (Code borrowed from github.com/linkedin/ambry)
   *
   * @param className the name of the class.
   * @param args      the arguments associated with the constructor of the
   *                  class that is to be invoked.
   * @return the instantiated object.
   * @throws ReflectiveOperationException if an associated constructor could
   * not be found or its instantiation fails.
   */
  public static Object getObj(String className, Object... args) throws ReflectiveOperationException {
    for (Constructor<?> ctor :
            Class.forName(className).getDeclaredConstructors()) {
      Class<?>[] paramTypes = ctor.getParameterTypes();
      if (paramTypes.length == args.length) {
        int i = 0;
        for (; i < args.length; i++) {
          if (!checkAssignable(paramTypes[i], args[i])) {
            break;
          }
        }
        if (i == args.length) {
          return ctor.newInstance(args);
        }
      }
    }
    throw buildNoConstructorException(className, args);
  }

  /**
   * Check if the given constructor parameter type is assignable from the
   * provided argument object.
   *
   * @param parameterType the {@link Class} of the constructor parameter.
   * @param arg           the argument to test.
   * @return {@code true} if it is assignable. Note: this will return true if
   * {@code arg} is {@code null}.
   */
  private static boolean checkAssignable(Class<?> parameterType, Object arg) {
    return arg == null || parameterType.isAssignableFrom(arg.getClass());
  }

  /**
   * Create a {@link NoSuchMethodException} for situations where a
   * constructor for a class that conforms to the provided
   * argument types was not found.
   *
   * @param className the class for which a suitable constructor was not found.
   * @param args      the constructor arguments.
   * @return the {@link NoSuchMethodException} to throw.
   */
  private static NoSuchMethodException buildNoConstructorException(String className, Object... args) {
    String argTypes =
            Stream.of(args).map(arg -> arg == null ? "null" :
                    arg.getClass().getName()).collect(Collectors.joining(", "));
    return new NoSuchMethodException(
            "No constructor found for " + className + " that takes arguments " +
                    "of types: " + argTypes);
  }

  /**
   * Read a properties file from the given path
   *
   * @param props    The Properties object to load into.
   * @param filename The path of the file to read.
   */
  public static void loadProps(Properties props, String filename) throws IOException {
    InputStream propStream = new FileInputStream(filename);
    props.load(propStream);
  }
}
