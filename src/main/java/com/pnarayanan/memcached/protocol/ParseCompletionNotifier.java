package com.pnarayanan.memcached.protocol;

import com.pnarayanan.memcached.common.Command;

/**
 * Notifier interface for when bytes are processed to form commands, or when
 * an error is encountered attempting to do so.
 */
public interface ParseCompletionNotifier {
  /**
   * Notification to use when a command is successfully decoded.
   *
   * @param command the {@link Command} that was decoded.
   * @throws InterruptedException
   */
  void onSuccess(Command command) throws InterruptedException;

  /**
   * Notification to use when an error was encountered while decoding.
   *
   * @param err the error response to be sent, if any.
   */
  void onError(byte[] err) throws InterruptedException;
}
