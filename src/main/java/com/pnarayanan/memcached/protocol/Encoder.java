package com.pnarayanan.memcached.protocol;

import com.pnarayanan.memcached.common.*;

import java.io.ByteArrayOutputStream;

/**
 * The Encoder provides the method to encode a response associated with a
 * command. This class does not maintain any
 * state, unlike the {@link Decoder}, as response bytes become available all
 * at once.
 */
public class Encoder {
  private static final byte[] LINE_END_BYTES = "\r\n".getBytes();
  private static final byte[] END_BYTES = "END\r\n".getBytes();
  private static final byte[] VALUE_BYTES = "VALUE ".getBytes();
  private static final byte[] STORED_BYTES = "STORED\r\n".getBytes();
  static final byte[] SET_GENERIC_ERROR_BYTES = Errors.getServerErrorStr(
          "Could not set").getBytes();
  static final byte[] GET_GENERIC_ERROR_BYTES = Errors.getServerErrorStr(
          "Could not get").getBytes();
  static final byte[] GENERIC_CLIENT_ERROR_BYTES = Errors.getClientErrorStr(
          "Unsupported command").getBytes();

  /**
   * Encode the given response from the Store.
   *
   * @param command  the {@link Command} associated with the response.
   * @param response the {@link StoreResponse} that is to be encoded.
   * @return the encoded byte array response
   */
  public static byte[] encodeResponse(Command command, StoreResponse response) {
    if (command.getType() == CommandType.SET) {
      return encodeSetResponse((SetCommand) command, (SetResponse) response);
    } else if (command.getType() == CommandType.GET) {
      return encodeGetResponse((GetCommand) command,
              (GetCompositeResponse) response);
    } else {
      return encodeClientError();
    }
  }

  /**
   * Encode the given {@link SetResponse}
   *
   * @param command  the {@link SetCommand} associated with the response.
   * @param response the {@link SetResponse} that is to be encoded.
   * @return the encoded byte array response
   */
  private static byte[] encodeSetResponse(SetCommand command,
                                          SetResponse response) {
    byte[] respArray = null;
    if (!command.isNoreply()) {
      if (response.getError() == null) {
        respArray = STORED_BYTES;
      } else {
        respArray = SET_GENERIC_ERROR_BYTES;
      }
    }
    return respArray;
  }

  /**
   * Encode the given {@link GetCompositeResponse}
   *
   * @param command  the {@link GetCommand} associated with the response.
   * @param response the {@link GetCompositeResponse} that is to be encoded.
   * @return the encoded byte array response
   */
  private static byte[] encodeGetResponse(GetCommand command,
                                          GetCompositeResponse response) {
    byte[] respArray;
    ByteArrayOutputStream out = new ByteArrayOutputStream();
    if (response.getError() == null) {
      for (GetResponse item : response.getGetResponseList()) {
        out.write(VALUE_BYTES, 0, VALUE_BYTES.length);
        StringBuilder sb = new StringBuilder();
        sb.append(item.getKey());
        sb.append(" ");
        sb.append(item.getFlags());
        sb.append(" ");
        sb.append(item.getLength());
        byte[] firstLine = sb.toString().getBytes();
        out.write(firstLine, 0, firstLine.length);
        out.write(LINE_END_BYTES, 0, LINE_END_BYTES.length);
        out.write(item.getValue().array(), 0, item.getLength());
        out.write(LINE_END_BYTES, 0, LINE_END_BYTES.length);
      }
      out.write(END_BYTES, 0, END_BYTES.length);
      respArray = out.toByteArray();
    } else {
      respArray = GET_GENERIC_ERROR_BYTES;
    }
    return respArray;
  }

  /**
   * Encode a client error response
   *
   * @return the encoded byte array response.
   */
  private static byte[] encodeClientError() {
    return GENERIC_CLIENT_ERROR_BYTES;
  }
}
