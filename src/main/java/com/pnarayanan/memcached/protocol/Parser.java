package com.pnarayanan.memcached.protocol;

import com.pnarayanan.memcached.common.Command;
import com.pnarayanan.memcached.common.CommandType;
import com.pnarayanan.memcached.common.GetCommand;
import com.pnarayanan.memcached.common.SetCommand;
import com.pnarayanan.memcached.utils.Utils;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

class Parser {
  private static final String SET_CMD_USAGE = "set <key> <flags> <exptime> " +
          "<length> [noreply]\r\n";
  private static final String GET_CMD_USAGE = "get <key>*\r\n";

  /**
   * Attempts to read bytes from the buffer and parses them to form a command.
   * If there are not enough bytes to form the command, the buffer's position
   * is reset to what it was when this method
   * was called.
   *
   * @param buf {@link ByteBuffer} from which to read the bytes of the command.
   * @return {@link ParseResult} which is the result of the parsing.
   */
  static ParseResult parseCommand(ByteBuffer buf) {
    ParseResult resp;
    // save the position to rewind in case there are not enough bytes to form
    // a command.
    buf.mark();
    try {
      String commandStr = Utils.maybeReadLine(buf);
      if (commandStr == null) {
        // A full line could not be read, reset position of the buffer to the
        // marked position and return.
        buf.reset();
        resp = new ParseResult(null, null);
      } else if (commandStr.startsWith("GET ") || commandStr.startsWith("get "
      )) {
        resp = parseGetCommand(commandStr);
      } else if (commandStr.startsWith("SET ") || commandStr.startsWith("set "
      )) {
        resp = parseSetCommand(commandStr);
      } else {
        resp = new ParseResult(null, Errors.getInvalidCommandErrorStr());
      }
    } catch (IllegalArgumentException e) {
      resp = new ParseResult(null, Errors.getClientErrorStr("Malformed " +
              "command, check for trailing '\\r\\n'"));
    }
    return resp;
  }

  /**
   * Parses a {@link CommandType#GET} command.
   *
   * @param commandStr the command String.
   * @return {@link ParseResult} containing the parsed get command, or an
   * error, or neither if there were not
   * enough bytes.
   */
  private static ParseResult parseGetCommand(String commandStr) {
    StringTokenizer st = new StringTokenizer(commandStr);
    if (st.countTokens() < 2) {
      return new ParseResult(null, Errors.getClientErrorStr("Invalid number " +
              "of arguments. Correct usage: " + GET_CMD_USAGE));
    }
    List<String> keys = new ArrayList<>();
    st.nextToken(); // the command itself.
    while (st.hasMoreTokens()) {
      keys.add(st.nextToken());
    }

    return new ParseResult(new GetCommand(keys), null);
  }

  /**
   * Parses a {@link CommandType#SET} command
   *
   * @param commandStr the command String
   * @return {@link ParseResult} containing the parsed set command, or an
   * error, or neither if there were not
   * enough bytes.
   */
  private static ParseResult parseSetCommand(String commandStr) {
    StringTokenizer st = new StringTokenizer(commandStr);
    int numTokens = st.countTokens();
    if (numTokens < 5 || numTokens > 6) {
      return new ParseResult(null, Errors.getClientErrorStr("Invalid number " +
              "of arguments. Correct usage: " + SET_CMD_USAGE));
    }
    try {
      st.nextToken(); // the command itself.
      String key = st.nextToken();
      int flags = Integer.parseInt(st.nextToken());
      int exptime = Integer.parseInt(st.nextToken());
      int length = Integer.parseInt(st.nextToken());
      boolean noreply = false;
      if (st.hasMoreTokens()) {
        if (st.nextToken().equalsIgnoreCase("noreply")) {
          noreply = true;
        } else {
          return new ParseResult(null, Errors.getClientErrorStr("Invalid " +
                  "argument(s), command was " + commandStr));
        }
      }
      // Increase length by two bytes to account for trailing '\r' and '\n'
      // in the data that is expected.
      return new ParseResult(new SetCommand(key, flags, exptime, length,
              noreply), null);
    } catch (NumberFormatException e) {
      return new ParseResult(null, Errors.getClientErrorStr("Invalid argument" +
              "(s), command was " + commandStr));
    }
  }
}

/**
 * An object representing the result of parsing bytes.
 */
class ParseResult {
  private final String error;
  private final Command command;

  /**
   * Constructor
   *
   * @param command the {@link Command} in the result, possibly null.
   * @param error   the error in the result, possibly null.
   */
  public ParseResult(Command command, String error) {
    this.command = command;
    this.error = error;
  }

  /**
   * @return the error, possibly null.
   */
  public String error() {
    return error;
  }

  /**
   * @return the {@link Command}, possibly null.
   */
  public Command getCommand() {
    return command;
  }
}
