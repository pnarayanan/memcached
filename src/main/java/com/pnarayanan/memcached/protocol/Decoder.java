package com.pnarayanan.memcached.protocol;

import com.pnarayanan.memcached.common.Command;

import java.nio.ByteBuffer;

/**
 * The Decoder is a stateful class that is responsible for decoding a request
 * out of the input bytes. It remembers the
 * state across invocations for the bytes read (that is whether a command was
 * partially parsed, or whether the data
 * associated with the command is being parsed, and so on) and uses the
 * {@link Parser} class to actually interpret the
 * bytes.
 * <p>
 * Here, a request represents the bytes composed of the command and its data
 * block, if any. It maintains state
 * across invocations of the {@link #processBytes(ByteBuffer)} method, and
 * the caller is expected to call this method
 * as and when new data needs to be decoded.
 * <p>
 * This class also takes in a {@link ParseCompletionNotifier} which will be
 * invoked as requests (commands) are
 * successfully decoded from the input, or if there is an error decoding
 * (such as if the input is malformed, or an
 * invalid command is encountered).
 */
public class Decoder {
  // Used to notify for every decoded command/error.
  private final ParseCompletionNotifier parseCompletionNotifier;
  // Maintains the state across calls.
  private State state;
  // The currently decoded command, or null if decoding is still in progress.
  private Command command;
  // The error string if the decoding of the current command led to an error.
  private String err;

  /**
   * Instantiate a Decoder
   *
   * @param parseCompletionNotifier A notifier to use for every request
   *                                parsed, or when an error is encountered.
   */
  public Decoder(ParseCompletionNotifier parseCompletionNotifier) {
    this.parseCompletionNotifier = parseCompletionNotifier;
    reinitialize();
  }

  /**
   * Reinitialize to decode next command.
   */
  private void reinitialize() {
    command = null;
    this.state = State.PARSING_COMMAND;
  }

  /**
   * Process bytes from the input buffer to form one or more commands. As
   * many commands as there are in the requestBuf
   * will be processed. This method returns when there are not enough bytes
   * in the buffer to completely decode a command.
   *
   * @param requestBuf the input {@link ByteBuffer} containing the bytes that
   *                   need to be decoded.
   * @throws InterruptedException if the current thread gets interrupted
   *                              while blocked. Note that this
   *                              method only blocks if the request
   *                              processing queue is full.
   */
  public void processBytes(ByteBuffer requestBuf) throws InterruptedException {
    boolean keepProcessing = true;
    while (keepProcessing) {
      if (state == State.PARSING_COMMAND) {
        readCommandBytes(requestBuf);
        if (command != null) {
          // the command has been fully parsed
          if (command.isReadyToSend()) {
            // this is a command with no data block.
            dispatchCommand();
          } else {
            // Command was fully read, but there is data to be read for this
            // command. Switch state.
            state = State.PARSING_DATA;
          }
        } else if (err != null) {
          dispatchError();
          // Keep parsing for the next command.
        } else {
          // if there was no error and if a command was not fully parsed,
          // there is no more work that can be done at this time.
          keepProcessing = false;
        }
      } else if (state == State.PARSING_DATA) {
        readDataBytes(requestBuf);
        if (command.isReadyToSend()) {
          dispatchCommand();
        } else {
          // there was not enough data in the buffer to fill up the data
          // block completely, any more work can
          // only be done if more bytes become available.
          keepProcessing = false;
        }
      }
    }
  }

  /**
   * Reads bytes to form a command if possible.
   *
   * @param requestBuf the buf to read bytes from.
   */
  private void readCommandBytes(ByteBuffer requestBuf) {
    ParseResult resp = Parser.parseCommand(requestBuf);
    err = resp.error();
    command = resp.getCommand();
  }

  /**
   * Reads bytes for the data block in the current command.
   *
   * @param requestBuf the buf to read bytes from.
   */
  private void readDataBytes(ByteBuffer requestBuf) {
    ByteBuffer dstBuf = command.getDataBuffer();
    int bytesToRead = command.getDataBuffer().remaining();
    // Only read byteToRead worth of data from requestBuf.
    // To do that, saved the current limit of requestBuf,
    // and set the limit so that no more than bytesToRead
    // is attempted to be read from requestBuf.
    int savedLimit = requestBuf.limit();
    if (requestBuf.remaining() > bytesToRead) {
      requestBuf.limit(requestBuf.position() + bytesToRead);
    }
    dstBuf.put(requestBuf);
    // Set back the original limit.
    requestBuf.limit(savedLimit);
  }

  /**
   * Dispatch the currently processed command by invoking parseCompleteNotifier.
   *
   * @throws InterruptedException
   */
  private void dispatchCommand() throws InterruptedException {
    command.prepare();
    parseCompletionNotifier.onSuccess(command);
    reinitialize();
  }

  /**
   * Dispatch the error, if the processing led to one.
   */
  private void dispatchError() throws InterruptedException {
    parseCompletionNotifier.onError(err.getBytes());
    reinitialize();
  }

  /**
   * Enum to track the state of the Decoder.
   */
  private enum State {
    PARSING_COMMAND, // Currently reading a command
    PARSING_DATA, // Currently reading data
  }
}
