package com.pnarayanan.memcached.protocol;

/**
 * Error response strings.
 */
class Errors {
  private static final String TRAILING_MARKER = "\r\n";

  static String getInvalidCommandErrorStr() {
    return "ERROR" + TRAILING_MARKER;
  }

  static String getClientErrorStr(String msg) {
    return "CLIENT_ERROR " + msg + TRAILING_MARKER;
  }

  static String getServerErrorStr(String msg) {
    return "SERVER_ERROR " + msg + TRAILING_MARKER;
  }
}


