package com.pnarayanan.memcached.config;

import java.util.Properties;

public class StoreConfig {

  /**
   * The factory class to use for the underlying store.
   */
  public final String storeFactory;
  private static final String storeFactoryConfigName =
          "memcached.store.factory";
  private static final String storeFactoryDefault =
          "com.pnarayanan.memcached.store.SegmentedLruStoreFactory";

  /**
   * The policy to follow for evicting entries from the store when the cache
   * is full.
   *
   * @todo: at this time, this is not configurable and is unused.
   */
  public final String evictionPolicy;
  private static final String evictionPolicyConfigName =
          "memcached.store.eviction.policy";
  private static final String evictionPolicyDefault = "LeastRecentlyUsed";

  /**
   * The maximum number of elements to be stored in the cache.
   */
  public final long maxElements;
  private static final String maxElementsConfigName =
          "memcached.store.max.elements";
  private static final long maxElementsDefault = 1024 * 1024;

  /**
   * The maximum memory size for the cache.
   */
  public final int maxMemory;
  private static final String maxMemoryConfigName =
          "memcached.store.max.memory";
  private static final int maxMemoryDefault = 1024 * 1024 * 1024;

  /**
   * The number of segments to use for the segmented store.
   */
  public final int numSegments;
  private static final String numSegmentsConfigName =
          "memcached.store.segmented.index.num.segments";
  private static final int numSegmentsDefault = 64;

  public StoreConfig(Properties props) {
    if (props.containsKey(storeFactoryConfigName)) {
      storeFactory = props.getProperty(storeFactoryConfigName);
    } else {
      storeFactory = storeFactoryDefault;
    }

    if (props.containsKey(evictionPolicyConfigName)) {
      evictionPolicy = props.getProperty(evictionPolicyConfigName);
    } else {
      evictionPolicy = evictionPolicyDefault;
    }

    if (props.containsKey(maxElementsConfigName)) {
      maxElements = Integer.parseInt(props.getProperty(maxElementsConfigName));
    } else {
      maxElements = maxElementsDefault;
    }

    if (props.containsKey(maxMemoryConfigName)) {
      maxMemory = Integer.parseInt(props.getProperty(maxMemoryConfigName));
    } else {
      maxMemory = maxMemoryDefault;
    }

    if (props.containsKey(numSegmentsConfigName)) {
      numSegments = Integer.parseInt(props.getProperty(numSegmentsConfigName));
    } else {
      numSegments = numSegmentsDefault;
    }
  }
}
