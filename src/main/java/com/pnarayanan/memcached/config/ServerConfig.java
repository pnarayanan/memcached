package com.pnarayanan.memcached.config;

import java.util.Properties;

/**
 * Configs for the Server
 */
public class ServerConfig {
  /**
   * The host address this server should bind and listen on.
   */
  public final String host;
  private static final String hostConfigName = "memcached.host";
  private static final String hostDefault = "localhost";

  /**
   * The port this server should bind and listen on.
   */
  public final int port;
  private static final String portConfigName = "memcached.port";
  private static final int portDefault = 11211;

  /**
   * The number of threads to handle connections.
   */
  public final int maxConnectionHandlerThreads;
  private static final String maxConnectionHandlerThreadsConfigName =
          "memcached.max.connection.handler.threads";
  private static final int maxConnectionHandlerThreadsDefault = 16;

  /**
   * The number of threads that handle requests.
   */
  public final int maxRequestHandlerThreads;
  private static final String maxRequestHandlerThreadsConfigName =
          "memcached.max.request.handler.threads";
  private static final int maxRequestHandlerThreadsDefault = 16;

  /**
   * The max size of the request queue.
   */
  public final int requestQueueSize;
  private static final String requestQueueSizeConfigName =
          "memcached.request.queue.size";
  private static final int requestQueueSizeDefault = 1024 * 1024;

  /**
   * Instantiate ServerConfig from the given properties.
   *
   * @param props the {@link Properties} containing the properties.
   */
  public ServerConfig(Properties props) {
    if (props.containsKey(hostConfigName)) {
      host = props.getProperty(hostConfigName);
    } else {
      host = hostDefault;
    }

    if (props.containsKey(portConfigName)) {
      port = Integer.parseInt(props.getProperty(portConfigName));
    } else {
      port = portDefault;
    }

    if (props.containsKey(maxConnectionHandlerThreadsConfigName)) {
      maxConnectionHandlerThreads =
              Integer.parseInt(props.getProperty(maxConnectionHandlerThreadsConfigName));
    } else {
      maxConnectionHandlerThreads = maxConnectionHandlerThreadsDefault;
    }

    if (props.containsKey(maxRequestHandlerThreadsConfigName)) {
      maxRequestHandlerThreads =
              Integer.parseInt(props.getProperty(maxRequestHandlerThreadsConfigName));
    } else {
      maxRequestHandlerThreads = maxRequestHandlerThreadsDefault;
    }

    if (props.containsKey(requestQueueSizeConfigName)) {
      requestQueueSize =
              Integer.parseInt(props.getProperty(requestQueueSizeConfigName));
    } else {
      requestQueueSize = requestQueueSizeDefault;
    }
  }
}
