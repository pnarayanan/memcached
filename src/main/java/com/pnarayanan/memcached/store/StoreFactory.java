package com.pnarayanan.memcached.store;

/**
 * Factory interface for factories that instantiate {@link Store} objects.
 */
public interface StoreFactory {
  public Store getStore() throws InstantiationException;
}
