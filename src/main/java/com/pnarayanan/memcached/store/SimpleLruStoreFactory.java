package com.pnarayanan.memcached.store;

import com.pnarayanan.memcached.config.StoreConfig;

/**
 * Factory class for {@link SimpleLruStore}
 */
public class SimpleLruStoreFactory implements StoreFactory {
  private final StoreConfig storeConfig;

  /**
   * Instantiate this factory
   *
   * @param storeConfig the {@link StoreConfig} to use.
   */
  public SimpleLruStoreFactory(StoreConfig storeConfig) {
    this.storeConfig = storeConfig;
  }

  /**
   * @return instantiated {@link SimpleLruStore}
   */
  public SimpleLruStore getStore() {
    return new SimpleLruStore(storeConfig);
  }
}
