package com.pnarayanan.memcached.store;

import com.pnarayanan.memcached.config.StoreConfig;

/**
 * Factory class for {@link SegmentedLruStore}
 */
public class SegmentedLruStoreFactory implements StoreFactory {
  private final StoreConfig storeConfig;

  /**
   * Instantiate this factory
   *
   * @param storeConfig the {@link StoreConfig} to use.
   */
  public SegmentedLruStoreFactory(StoreConfig storeConfig) {
    this.storeConfig = storeConfig;
  }

  /**
   * @return instantiated {@link SegmentedLruStore}
   */
  public SegmentedLruStore getStore() {
    return new SegmentedLruStore(storeConfig);
  }
}

