package com.pnarayanan.memcached.store;

import com.pnarayanan.memcached.common.GetResponse;
import com.pnarayanan.memcached.common.SetResponse;

import java.nio.ByteBuffer;

/**
 * Store interface represents the Cache.
 */
interface Store {
  /**
   * Handle a Set operation.
   *
   * @param key     the key associated with the operation.
   * @param flags   the flags associated with the operation.
   * @param exptime the expirty time associated with the operation.
   * @param length  the length of the data block.
   * @param value   the data block.
   * @return {@link SetResponse} the response of setting.
   */
  SetResponse set(String key, int flags, int exptime, int length,
                  ByteBuffer value);

  /**
   * Handle a get operation.
   *
   * @param key the key associated with the Get operation.
   * @return {@link GetResponse} containing the response for this Get
   * operatin if there is one; null if the key is
   * absent in the store.
   */
  GetResponse get(String key);
}
