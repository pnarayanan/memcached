package com.pnarayanan.memcached.store;

import com.pnarayanan.memcached.common.GetResponse;
import com.pnarayanan.memcached.common.SetResponse;
import com.pnarayanan.memcached.config.StoreConfig;

import java.nio.ByteBuffer;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.logging.Logger;

/**
 * Simple concurrent store implementation that uses a single synchronized
 * LinkedHashMap as the index.
 */
public class SimpleLruStore implements Store {
  private final Map<String, IndexRecord> index;
  private static final int INITIAL_CAPACITY = 1024 * 1024;
  private static final float LOAD_FACTOR = 0.75f;

  private static final Logger logger =
          Logger.getLogger(SimpleLruStore.class.getName());

  /**
   * Instantiate a SimpleLruStore
   *
   * @param storeConfig {@link StoreConfig} to use.
   */
  SimpleLruStore(StoreConfig storeConfig) {
    this(storeConfig.maxElements);
  }

  /**
   * Instantiate a SimpleLruStore
   *
   * @param maxElements the max elements to store in this cache.
   */
  protected SimpleLruStore(long maxElements) {
    // initialize LinkedHashMap with access order, so that eldest entry will
    // be the least recently used one.
    index = Collections.synchronizedMap(new LinkedHashMap<String,
            IndexRecord>(INITIAL_CAPACITY, LOAD_FACTOR, true) {
      @Override
      protected boolean removeEldestEntry(Map.Entry<String, IndexRecord> eldest) {
        return size() > maxElements;
      }
    });
  }

  /**
   * Handle a Set operation.
   *
   * @param key     the key associated with the operation.
   * @param flags   the flags associated with the operation.
   * @param exptime the expirty time associated with the operation.
   * @param length  the length of the data block.
   * @param value   the data block.
   * @return {@link SetResponse} the response of setting.
   */
  @Override
  public SetResponse set(String key, int flags, int exptime, int length,
                         ByteBuffer value) {
    IndexRecord record = new IndexRecord(flags, exptime, length, value);
    index.put(key, record);
    return new SetResponse(null);
  }

  /**
   * Handle a get operation.
   *
   * @param key the key associated with the Get operation.
   * @return {@link GetResponse} containing the response for this Get
   * operatin if there is one; null if the key is
   * absent in the store.
   */
  @Override
  public GetResponse get(String key) {
    IndexRecord record = index.get(key);
    if (record != null) {
      return new GetResponse(key, record.flags, record.exptime, record.length
              , record.value);
    } else {
      return null;
    }
  }

  /**
   * IndexRecord is the value that is stored in the hash map against keys.
   */
  static class IndexRecord {
    int flags;
    int exptime;
    int length;
    ByteBuffer value;

    /**
     * Instantiate an IndexRecord
     *
     * @param flags   the flags associated with this record.
     * @param exptime the exprity time associated with this record.
     * @param length  the length of the value associated with this record.
     * @param value   the value associated with this record.
     */
    IndexRecord(int flags, int exptime, int length, ByteBuffer value) {
      this.flags = flags;
      this.exptime = exptime;
      this.length = length;
      this.value = value;
    }
  }
}
