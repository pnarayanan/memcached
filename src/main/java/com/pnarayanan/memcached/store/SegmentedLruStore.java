package com.pnarayanan.memcached.store;

import com.pnarayanan.memcached.common.GetResponse;
import com.pnarayanan.memcached.common.SetResponse;
import com.pnarayanan.memcached.config.StoreConfig;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;

/**
 * SegmentedLruStore uses multiple index segments for the index. A given key
 * will map to one of the indexes. This can
 * provide better performance since operations across index segments do not
 * contend with each other.
 * <p>
 * The segmenting is done by simply using {@link SimpleLruStore} for each
 * segment.
 */
public class SegmentedLruStore implements Store {
  // no modifications after initialization, only access, so thread-safe.
  private final List<SimpleLruStore> segments;
  private static final Logger logger =
          Logger.getLogger(SegmentedLruStore.class.getName());

  /**
   * Instantiate a SegmentedLruStore
   *
   * @param storeConfig the {@link StoreConfig} containing configurations for
   *                   the Store.
   */
  SegmentedLruStore(StoreConfig storeConfig) {
    int numSegments = storeConfig.numSegments;
    long maxElementsPerSegment = storeConfig.maxElements / numSegments;
    ArrayList<SimpleLruStore> segments = new ArrayList<>(numSegments);
    for (int i = 0; i < numSegments; i++) {
      segments.add(new SimpleLruStore(maxElementsPerSegment));
    }
    this.segments = Collections.unmodifiableList(segments);
  }

  /**
   * Handle a Set operation.
   *
   * @param key     the key associated with the operation.
   * @param flags   the flags associated with the operation.
   * @param exptime the expirty time associated with the operation.
   * @param length  the length of the data block.
   * @param value   the data block.
   * @return {@link SetResponse} the response of setting.
   */
  @Override
  public SetResponse set(String key, int flags, int exptime, int length,
                         ByteBuffer value) {
    return getSegmentForKey(key).set(key, flags, exptime, length, value);
  }

  /**
   * Handle a get operation.
   *
   * @param key the key associated with the Get operation.
   * @return {@link GetResponse} containing the response for this Get
   * operatin if there is one; null if the key is
   * absent in the store.
   */
  @Override
  public GetResponse get(String key) {
    return getSegmentForKey(key).get(key);
  }

  /**
   * Get the Index Segment associated with this key.
   *
   * @param key the key for which the associated segment is to be returned.
   * @return the associated {@link SimpleLruStore} segment.
   */
  private SimpleLruStore getSegmentForKey(String key) {
    int segmentId = Math.abs(key.hashCode()) % segments.size();
    return segments.get(segmentId);
  }
}
