package com.pnarayanan.memcached.store;

import com.pnarayanan.memcached.common.*;
import com.pnarayanan.memcached.config.StoreConfig;
import com.pnarayanan.memcached.utils.Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * StoreManager handles commands to {@link Store}.
 */
public class StoreManager {
  private final Store store;
  private static final Logger logger =
          Logger.getLogger(StoreManager.class.getName());

  /**
   * Instantiate StoreManager
   *
   * @param storeConfig the {@link StoreConfig} containing the configs for
   *                    store.
   * @throws ReflectiveOperationException if a store could not be instantiated.
   */
  public StoreManager(StoreConfig storeConfig) throws ReflectiveOperationException {
    store = ((StoreFactory) Utils.getObj(storeConfig.storeFactory,
            storeConfig)).getStore();
  }

  /**
   * Handles a command and returns the response from handling.
   *
   * @param command the {@link Command} to handle.
   * @return {@link StoreResponse} containing the response, or null if there
   * is no response.
   */
  public StoreResponse handleCommand(Command command) {
    StoreResponse storeResponse;
    if (command.getType() == CommandType.SET) {
      storeResponse = handleSet((SetCommand) command);
    } else if (command.getType() == CommandType.GET) {
      storeResponse = handleGet((GetCommand) command);
    } else {
      storeResponse = () -> "Unknown command";
    }
    return storeResponse;
  }

  /**
   * Handles a SET command and returns the response from handling.
   *
   * @param command the {@link SetCommand} to handle.
   * @return {@link SetResponse} containing the response.
   */
  private SetResponse handleSet(SetCommand command) {
    SetResponse resp;
    try {
      resp = store.set(command.getKey(), command.getFlags(),
              command.getExptime(), command.getLength(),
              command.getDataBuffer());
    } catch (Exception e) {
      logger.log(Level.WARNING, e.getMessage());
      resp = new SetResponse("" + e.getMessage());
    }
    return resp;
  }

  /**
   * Handles a GET command and returns the response from handling.
   *
   * @param command the {@link GetCommand} to handle.
   * @return {@link GetCompositeResponse} containing one or more
   * {@link GetResponse}.
   */
  private GetCompositeResponse handleGet(GetCommand command) {
    GetCompositeResponse resp;
    List<GetResponse> result = new ArrayList<>();
    try {
      for (String key : command.getKeys()) {
        GetResponse getResp = store.get(key);
        if (getResp != null) {
          result.add(getResp);
        }
      }
      resp = new GetCompositeResponse(result, null);
    } catch (Exception e) {
      logger.log(Level.WARNING, e.getMessage());
      resp = new GetCompositeResponse(null, "" + e.getMessage());
    }
    return resp;
  }
}
