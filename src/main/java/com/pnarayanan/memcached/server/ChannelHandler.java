package com.pnarayanan.memcached.server;

import com.pnarayanan.memcached.common.Command;
import com.pnarayanan.memcached.protocol.Decoder;
import com.pnarayanan.memcached.protocol.ParseCompletionNotifier;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * ChannelHandler class does the work associated with reading and writing
 * from a channel. It is instantiated by the
 * {@link ConnectionHandler} whenever a new connection is established, and a
 * channel is formed.
 * It uses a {@link Decoder} object to decode the incoming bytes. The methods
 * in this class are called whenever a read
 * or write operation needs to happen on the channel.
 */
public class ChannelHandler implements AutoCloseable {
  private static final Logger logger =
          Logger.getLogger(ChannelHandler.class.getName());
  private boolean active;
  private final SocketChannel byteChannel;
  private final RequestResponseChannel requestResponseChannel;
  private final ConnectionHandler connectionHandler;
  private final ByteBuffer requestBuf;
  private final LinkedList<ByteBuffer> responseBufs = new LinkedList<>();
  private Decoder decoder;

  private static final int BUF_SIZE = 8192; // this can be made configurable.

  /**
   * Instantiate a ChannelHandler
   *
   * @param byteChannel            the {@link SocketChannel} associated with
   *                               this class, and which is used to read and
   *                               write bytes.
   * @param requestResponseChannel the {@link RequestResponseChannel} used
   *                               for enqueueing fully decoded requests and
   *                               dequeueing responses.
   * @param connectionHandler      the {@link ConnectionHandler} class that
   *                               is managing the connection associated with
   *                               this
   *                               channel.
   */
  ChannelHandler(SocketChannel byteChannel,
                 RequestResponseChannel requestResponseChannel,
                 ConnectionHandler connectionHandler) {
    this.byteChannel = byteChannel;
    this.requestResponseChannel = requestResponseChannel;
    this.connectionHandler = connectionHandler;
    requestBuf = ByteBuffer.allocate(BUF_SIZE);
    active = true;
  }

  /**
   * Instantiates the {@link Decoder} if it is not yet instantiated.
   */
  private void instantiateDecoderIfNeeded() {
    if (decoder == null) {
      decoder = new Decoder(new ParseCompletionNotifier() {
        @Override
        public void onSuccess(Command command) throws InterruptedException {
          requestResponseChannel.enqueueRequest(new Request(command,
                  ChannelHandler.this));
        }

        @Override
        public void onError(byte[] err) throws InterruptedException {
          requestResponseChannel.enqueueResponse(err, new Request(null,
                  ChannelHandler.this));
        }
      });
    }
  }

  /**
   * Called whenever a read is to be performed on the associated
   * {@link SocketChannel}
   * <p>
   * When this method is called, the holding buffer {@code buf} may be
   * partially filled, and {@code bytesRead} will
   * indicate how many bytes are filled.
   *
   * @return if the channel will not be read from further. This is usually
   * the case if reading from this channel
   * reached an End-of-Stream. It can also happen if an exception was
   * encountered and reading from this channel was
   * disabled permanently.
   */
  boolean onRead() {
    if (!active) {
      return true;
    }
    instantiateDecoderIfNeeded();
    try {
      int bytesRead;
      do {
        bytesRead = byteChannel.read(requestBuf);
        if (bytesRead > 0) {
          // More data was read
          requestBuf.flip();
          decoder.processBytes(requestBuf);
          requestBuf.compact();
        }
      } while (bytesRead > 0);
      if (bytesRead == -1) {
        // End of stream was reached, the connection is closed.
        active = false;
      }
    } catch (IOException e) {
      logger.log(Level.WARNING, e.getMessage(), e);
      active = false;
    } catch (InterruptedException e) {
      Thread.currentThread().interrupt();
      active = false;
    }
    return !active;
  }

  /**
   * Enqueues the given response to be written to the channel.
   *
   * @param response the byte array that is to be written to this channel.
   * @return true if all pending writes on this channel are complete, false
   * otherwise.
   */
  boolean write(byte[] response) throws IOException {
    responseBufs.add(ByteBuffer.wrap(response));
    return doWrite();
  }

  /**
   * Write an enqueued response (or continue a previous write) to the channel
   *
   * @return true if all enqueued writes for this channel are complete.
   */
  boolean doWrite() throws IOException {
    while (!responseBufs.isEmpty()) {
      ByteBuffer curBuf = responseBufs.peek();
      int written;
      do {
        written = byteChannel.write(curBuf);
      } while (written > 0 && curBuf.hasRemaining());
      if (curBuf.hasRemaining()) {
        return false;
      } else {
        responseBufs.poll();
      }
    }
    return true;
  }

  /**
   * @return the {@link SocketChannel} associated with this class.
   */
  SocketChannel getChannel() {
    return byteChannel;
  }

  /**
   * @return whether this channel is active.
   */
  boolean isActive() {
    return active;
  }

  /**
   * @return the {@link ConnectionHandler} associated with this class.
   */
  ConnectionHandler getConnectionHandler() {
    return connectionHandler;
  }


  /**
   * Close this channel. After this method is called, this channel will no
   * longer be active.
   */
  @Override
  public void close() {
    active = false;
  }
}
