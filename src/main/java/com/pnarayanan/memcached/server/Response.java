package com.pnarayanan.memcached.server;

/**
 * The server deals with requests and responses. The Response class has an
 * associated {@link ChannelHandler} with it,
 * which is associated with the channel to which the response is meant.
 */
class Response {
  private final byte[] payload;
  private final ChannelHandler channelHandler;

  /**
   * Instantiate a Response object.
   *
   * @param payload        the bytes associated with the response.
   * @param channelHandler the {@link ChannelHandler} associated with the
   *                       channel to which this response has to be sent.
   */
  Response(byte[] payload, ChannelHandler channelHandler) {
    this.payload = payload;
    this.channelHandler = channelHandler;
  }

  /**
   * @return returns the payload
   */
  byte[] getPayload() {
    return payload;
  }

  /**
   * @return returns the {@link ChannelHandler}
   */
  ChannelHandler getChannelHandler() {
    return channelHandler;
  }
}
