package com.pnarayanan.memcached.server;

import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * RequestResponseChannel is a medium (not a byte channel) for sending and
 * receiving request and response objects.
 * Internally it manages a queue for requests, and queues per
 * ConnectionHandlers for responses.
 */
class RequestResponseChannel {
  // A common queue for requests.
  private final ArrayBlockingQueue<Request> requestQueue;
  // Response queues per connection handler.
  private final List<LinkedBlockingQueue<Response>> responseQueues;

  RequestResponseChannel(ArrayBlockingQueue<Request> requestQueues,
                         List<LinkedBlockingQueue<Response>> responseQueues) {
    this.requestQueue = requestQueues;
    this.responseQueues = responseQueues;
  }

  /**
   * Enqueue requests for processing by one of the
   * {@link RequestResponseHandler} threads.
   * Blocks if queue is full, since it is bounded (basic rate limiting).
   *
   * @param request the {@link Request} to be enqueued.
   * @throws InterruptedException if the thread gets interrupted while blocked.
   */
  void enqueueRequest(Request request) throws InterruptedException {
    requestQueue.put(request);

  }

  /**
   * Dequeue request for processing. Blocks for requests, if empty.
   *
   * @return the dequeued request.
   * @throws InterruptedException if the thread gets interrupted while blocked.
   */
  Request dequeueRequest() throws InterruptedException {
    return requestQueue.take();
  }

  /**
   * Enqueues response to the queue associated with the connection handler
   * for the associated request.
   *
   * @param responseArray the byte array containing the actual bytes of the
   *                      response.
   * @param request       the associated {@link Request}
   * @throws InterruptedException if the thread gets interrupted while blocked.
   */
  void enqueueResponse(byte[] responseArray, Request request) throws InterruptedException {
    ChannelHandler channelHandler = request.getChannelHandler();
    Response response = new Response(responseArray, channelHandler);
    responseQueues.get(channelHandler.getConnectionHandler().getId()).put(response);
    channelHandler.getConnectionHandler().onResponse();
  }

  /**
   * Dequeues one response, if any, from the queue associated with the
   * ConnectionHandler with the given id, and returns it.
   *
   * @param connectionHandlerId the id of the Connection Handler from whose
   *                            queue a response is to be dequeued.
   * @return the dequeued response if there is one, null otherwise. This
   * method does not block.
   */
  Response dequeueResponse(int connectionHandlerId) {
    return responseQueues.get(connectionHandlerId).poll();
  }
}
