package com.pnarayanan.memcached.server;

import com.pnarayanan.memcached.config.ServerConfig;
import com.pnarayanan.memcached.config.StoreConfig;
import com.pnarayanan.memcached.store.StoreManager;
import com.pnarayanan.memcached.utils.Utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The Server class bootstraps the Memcached server. It sets up all the
 * handlers, thread pools, and other objects.
 */
public class Server {
  private ConnectionAcceptor connectionAcceptor;
  private ExecutorService requestHandlerPool;
  private ExecutorService connectionHandlerPool;
  private static final Logger logger = Logger.getLogger(Server.class.getName());

  /**
   * Entry point. Process the configuration file, if present (as indicated by
   * the arguments), and starts the server.
   *
   * @param args the arguments to the program.
   */
  public static void main(String[] args) {
    System.out.println("Started Memcached Server");
    String propsFilePath = null;
    if (args.length > 0) {
      propsFilePath = args[0];
    }
    if (args.length > 1) {
      System.err.println("Usage: memcached [properties_file_path]");
      System.exit(1);
    }

    Properties props = new Properties();
    if (propsFilePath != null) {
      try {
        Utils.loadProps(props, propsFilePath);
      } catch (IOException e) {
        throw new IllegalArgumentException("Invalid properties path: " + propsFilePath);
      }
    }
    AtomicBoolean keepRunning = new AtomicBoolean(true);
    try {
      Server server = new Server();
      server.start(props, keepRunning);
      server.awaitShutdown();
    } catch (Exception e) {
      keepRunning.set(false);
      logger.log(Level.WARNING, e.getMessage());
    }
  }

  /**
   * Bootstrap the server. Instantiate all the handlers, and thread pools.
   *
   * @param props       the {@link Properties} object containing configs.
   * @param keepRunning whether to keep running.
   * @throws Exception if any error is encountered.
   */
  void start(Properties props, AtomicBoolean keepRunning) throws Exception {
    ServerConfig serverConfig = new ServerConfig(props);
    StoreManager storeManager = new StoreManager(new StoreConfig(props));
    List<ConnectionHandler> connectionHandlers =
            new ArrayList<>(serverConfig.maxConnectionHandlerThreads);
    ArrayBlockingQueue<Request> requestQueue =
            new ArrayBlockingQueue<>(serverConfig.requestQueueSize, true);
    List<LinkedBlockingQueue<Response>> responseQueues = new ArrayList<>();
    RequestResponseChannel requestResponseChannel =
            new RequestResponseChannel(requestQueue, responseQueues);

    // Start a thread pool for request handlers that execute commands and get
    // responses.
    requestHandlerPool =
            Executors.newFixedThreadPool(serverConfig.maxRequestHandlerThreads);
    for (int i = 0; i < serverConfig.maxRequestHandlerThreads; i++) {
      RequestResponseHandler requestResponseHandler =
              new RequestResponseHandler(requestResponseChannel, storeManager
                      , keepRunning);
      requestHandlerPool.submit(requestResponseHandler);
    }

    connectionHandlerPool =
            Executors.newFixedThreadPool(serverConfig.maxConnectionHandlerThreads);
    for (int i = 0; i < serverConfig.maxConnectionHandlerThreads; i++) {
      ConnectionHandler connectionHandler =
              new ConnectionHandler(requestResponseChannel, i, keepRunning);
      connectionHandlers.add(connectionHandler);
      responseQueues.add(new LinkedBlockingQueue<>());
      connectionHandlerPool.submit(connectionHandler);
    }

    connectionAcceptor =
            new ConnectionAcceptor(serverConfig.host, serverConfig.port,
                    connectionHandlers, keepRunning);
    Thread acceptorThread = new Thread(connectionAcceptor);
    acceptorThread.start();
  }

  void awaitShutdown() throws InterruptedException {
    connectionAcceptor.awaitShutdown();
    requestHandlerPool.awaitTermination(10, TimeUnit.SECONDS);
    connectionHandlerPool.awaitTermination(10, TimeUnit.SECONDS);
  }
}
