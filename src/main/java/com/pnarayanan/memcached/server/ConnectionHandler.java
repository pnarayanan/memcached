package com.pnarayanan.memcached.server;

import java.io.IOException;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;

import static java.nio.channels.SelectionKey.OP_READ;
import static java.nio.channels.SelectionKey.OP_WRITE;

/**
 * A ConnectionHandler class runs in its own thread and manages a set of
 * client connections. These connections are
 * handed over to it by the {@link ConnectionAcceptor} thread. There can be
 * many ConnectionHandler threads running and each
 * will manage a separate set of connections accepted by the
 * {@link ConnectionAcceptor} thread.
 * <p>
 * The ConnectionHandler handles uses a {@link Selector} to react on read and
 * write activities on the connections it
 * manages. For every associated connection, it instantiates a
 * {@link ChannelHandler} to do the actual read and writes.
 */
class ConnectionHandler implements Runnable {
  private final int id;
  private final Selector selector;
  private final Map<SocketChannel, ChannelHandler> channelToHandler =
          new ConcurrentHashMap<>();
  private final RequestResponseChannel requestResponseChannel;
  private final ConcurrentLinkedQueue<SocketChannel> pendingConnections =
          new ConcurrentLinkedQueue<>();
  private final AtomicBoolean keepRunning;
  private static final Logger logger =
          Logger.getLogger(ConnectionHandler.class.getName());
  private static final int POLL_TIMEOUT = 3000;

  /**
   * Instantiate a ConnectionHandler
   *
   * @param requestResponseChannel the {@link RequestResponseChannel} used to
   *                               dequeue responses to be written out.
   * @param id                     the integer id associated with this handler.
   * @param keepRunning            whether to continue running.
   * @throws IOException if a network error is encountered.
   */
  ConnectionHandler(RequestResponseChannel requestResponseChannel, int id,
                    AtomicBoolean keepRunning) throws IOException {
    this.id = id;
    this.requestResponseChannel = requestResponseChannel;
    this.keepRunning = keepRunning;
    selector = Selector.open();
  }

  /**
   * Runs in a thread to handle read and write activities on all the client
   * connections managed by this handler.
   */
  @Override
  public void run() {
    try {
      while (!Thread.currentThread().isInterrupted() && keepRunning.get()) {
        if (selector.isOpen()) {
          registerPendingConnections();
          processResponses();
          selector.select(POLL_TIMEOUT);
          Iterator<SelectionKey> iter = selector.selectedKeys().iterator();
          while (iter.hasNext()) {
            SelectionKey selectionKey = iter.next();
            try {
              if (selectionKey.isValid()) {
                if (selectionKey.isReadable()) {
                  //System.out.println("++OnReadable");
                  onReadable(selectionKey);
                } else if (selectionKey.isWritable()) {
                  onWritable(selectionKey);
                }
              }
            } catch (IOException e) {
              logger.log(Level.WARNING,
                      "Exception encountered: " + e.getMessage());
              closeChannel((SocketChannel) selectionKey.channel(),
                      selectionKey);
            }
            iter.remove();
          }
        } else {
          Thread.currentThread().interrupt();
        }
      }
    } catch (Exception e) {
      logger.log(Level.WARNING, "Exception encountered: " + e.getMessage());
    } finally {
      close();
    }
  }

  /**
   * Closes this handler and associated selector, channels and keys.
   */
  private void close() {
    try {
      selector.close();
    } catch (Exception e) {
      logger.log(Level.WARNING, "Exception encountered: " + e.getMessage());
    }
  }

  /**
   * Register all pending connections that have been accepted and assigned to
   * this handler by the
   * {@link ConnectionAcceptor}
   *
   * @throws IOException if a network error is encountered.
   */
  private void registerPendingConnections() throws IOException {
    if (pendingConnections.size() > 0) {
      SocketChannel channel = pendingConnections.poll();
      channel.configureBlocking(false);
      channel.socket().setTcpNoDelay(true);
      ChannelHandler channelHandler = new ChannelHandler(channel,
              requestResponseChannel, this);
      channelToHandler.put(channel, channelHandler);
      // Start by only waiting for requests
      channel.register(selector, OP_READ);
    }
  }

  /**
   * Closes the given channel and cancels the selection key
   *
   * @param clientChannel the {@link SocketChannel} to close.
   * @param key           the {@link SelectionKey} associated with the
   *                      channel that is to be cancelled.
   * @throws IOException if a network error is encountered.
   */
  private void closeChannel(SocketChannel clientChannel, SelectionKey key) throws IOException {
    ChannelHandler handler = channelToHandler.remove(clientChannel);
    clientChannel.close();
    handler.close();
    key.cancel();
  }

  /**
   * Process any queued responses for this handler.
   *
   * @throws IOException if a network error is encountered.
   */
  private void processResponses() throws IOException {
    while (true) {
      Response response = requestResponseChannel.dequeueResponse(id);
      if (response == null) {
        break;
      } else {
        ChannelHandler channelHandler = response.getChannelHandler();
        if (channelHandler.isActive()) {
          SocketChannel channel = channelHandler.getChannel();
          SelectionKey key = channel.keyFor(selector);
          try {
            boolean channelWriteComplete =
                    channelHandler.write(response.getPayload());
            if (!channelWriteComplete) {
              // if channel write could not complete, register the channel
              // for WRITE so when the channel is ready for
              // more writes, writing can be continued.
              key.interestOps(key.interestOps() | OP_WRITE);
            }
          } catch (IOException e) {
            logger.log(Level.WARNING, e.getMessage());
            closeChannel(channel, key);
          }
        }
      }
    }
  }

  /**
   * Called by the main thread when there is more data to be read for the
   * channel associated with the given key.
   * Invoke the read method on the associated {@link ChannelHandler}
   *
   * @param selectionKey the {@link SelectionKey} associated with the channel
   *                     that has the read event.
   * @throws IOException if a network error is encountered.
   */
  private void onReadable(SelectionKey selectionKey) throws IOException {
    SocketChannel clientChannel = (SocketChannel) selectionKey.channel();
    boolean channelEosReached = channelToHandler.get(clientChannel).onRead();
    if (channelEosReached) {
      closeChannel(clientChannel, selectionKey);
    }
  }

  /**
   * Called by the main thread when the channel associated with the given key
   * is ready for more data to be written to it.
   *
   * @param selectionKey the {@link SelectionKey} associated with the channel
   *                     that has the write event.
   * @throws IOException if a network error is encountered.
   */
  private void onWritable(SelectionKey selectionKey) throws IOException {
    SocketChannel clientChannel = (SocketChannel) selectionKey.channel();
    boolean channelWriteComplete =
            channelToHandler.get(clientChannel).doWrite();
    if (channelWriteComplete) {
      // if channel write has completed for now, deregister the WRITE
      // interest. Any new writes to the channel will be
      // handled in processResponses(), (and if those writes do not complete,
      // that method will register WRITE interest
      // again.
      selectionKey.interestOps(selectionKey.interestOps() & ~OP_WRITE);
    }
  }

  /**
   * This method is called by the {@link ConnectionAcceptor} thread to add
   * accepted client connections.
   *
   * @param channel the {@link SocketChannel} associated with the accepted
   *                connection that is to be managed by this
   *                handler.
   */
  void add(SocketChannel channel) {
    pendingConnections.add(channel);
    selector.wakeup();
  }

  /**
   * To be called whenever a new response is added to the queue. The selector
   * needs to be woken up, because it could
   * be sleeping in the main thread waiting for events. By waking up, it is
   * given a chance to process new responses
   * immediately.
   */
  void onResponse() {
    selector.wakeup();
  }

  /**
   * @return the id associated with this handler.
   */
  int getId() {
    return id;
  }
}

