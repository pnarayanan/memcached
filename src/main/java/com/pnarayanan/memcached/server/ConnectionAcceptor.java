package com.pnarayanan.memcached.server;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * ConnectionAcceptor accepts all new connections. It uses a {@link Selector}
 * and reacts to connection establishment
 * events.
 */
public class ConnectionAcceptor implements Runnable {
  private final CountDownLatch shutdownLatch = new CountDownLatch(1);
  private final Selector selector;
  private final ServerSocketChannel serverChannel;
  private final List<ConnectionHandler> connectionHandlers;
  private final String host;
  private final int port;
  private final AtomicBoolean keepRunning;
  private int acceptedConnections = 0;
  private static final Logger logger =
          Logger.getLogger(ConnectionAcceptor.class.getName());
  private static final int POLL_TIMEOUT = 3000;

  /**
   * Instantiate a ConnectionAcceptor
   *
   * @param host               the host address to listen on
   * @param port               the port to listen on
   * @param connectionHandlers the pool of {@link ConnectionHandler}s that
   *                           will be used for delegating handling of
   *                           established connections.
   * @param keepRunning        indicates whether this thread should continue
   *                           running.
   * @throws IOException if there is a network error during initialization.
   */
  ConnectionAcceptor(String host, int port,
                     List<ConnectionHandler> connectionHandlers,
                     AtomicBoolean keepRunning) throws IOException {
    this.host = host;
    this.port = port;
    this.selector = Selector.open();
    this.serverChannel = ServerSocketChannel.open();
    this.connectionHandlers = connectionHandlers;
    this.keepRunning = keepRunning;
    initializeServerChannel();
  }

  /**
   * Runs in its own thread until shutdown to accept new connections and hand
   * over accepted connections to one of the
   * {@link ConnectionHandler} objects.
   */
  @Override
  public void run() {
    try {
      int currentHandlerId = 0;
      while (!Thread.currentThread().isInterrupted() && keepRunning.get()) {
        if (selector.isOpen()) {
          selector.select(POLL_TIMEOUT);
          Iterator<SelectionKey> iter = selector.selectedKeys().iterator();
          while (iter.hasNext()) {
            SelectionKey selectionKey = iter.next();
            if (selectionKey.isValid() && selectionKey.isAcceptable()) {
              SocketChannel ch = serverChannel.accept();
              if (ch != null) {
                connectionHandlers.get(currentHandlerId).add(ch);
                acceptedConnections++;
                currentHandlerId =
                        (currentHandlerId + 1) % connectionHandlers.size();
              }
            }
            iter.remove();
          }
        } else {
          Thread.currentThread().interrupt();
        }
      }
    } catch (IOException e) {
      logger.log(Level.WARNING, "Exception while accepting new connections " +
              "\n" + e.getMessage());
    } finally {
      keepRunning.set(false);
      shutdownLatch.countDown();
    }
  }

  /**
   * Initialize the channel associated with this server.
   *
   * @throws IOException if a network error is encountered.
   */
  private void initializeServerChannel() throws IOException {
    serverChannel.socket().setReuseAddress(true);
    serverChannel.bind(new InetSocketAddress(host, port));
    serverChannel.configureBlocking(false);
    serverChannel.register(selector, serverChannel.validOps(), null);
  }

  /**
   * Blocks on the main thread shutting down.
   *
   * @throws InterruptedException if this thread gets interrupted while blocked.
   */
  void awaitShutdown() throws InterruptedException {
    shutdownLatch.await();
  }

  /**
   * @return return the number of connections accepted so far.
   */
  int acceptedConnections() {
    return acceptedConnections;
  }
}
