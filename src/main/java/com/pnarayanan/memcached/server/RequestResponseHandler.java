package com.pnarayanan.memcached.server;

import com.pnarayanan.memcached.common.StoreResponse;
import com.pnarayanan.memcached.protocol.Encoder;
import com.pnarayanan.memcached.store.StoreManager;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class handles requests and responses to the {@link StoreManager}. It
 * runs in its own thread, dequeues requests
 * to be handled, sends commands associated with those requests to the store
 * and enqueues responses received.
 */
public class RequestResponseHandler implements Runnable {
  private final RequestResponseChannel requestResponseChannel;
  private final StoreManager storeManager;
  private final AtomicBoolean keepRunning;
  private static final Logger logger =
          Logger.getLogger(RequestResponseHandler.class.getName());

  /**
   * Instantiate a RequestResponseHandler object.
   *
   * @param requestResponseChannel the {@link RequestResponseChannel} used
   *                               for dequeueing requests and enqueueing
   *                               responses.
   * @param storeManager           the {@link StoreManager} to send commands to.
   * @param keepRunning            whether this thread should keep running.
   */
  RequestResponseHandler(RequestResponseChannel requestResponseChannel,
                         StoreManager storeManager, AtomicBoolean keepRunning) {
    this.requestResponseChannel = requestResponseChannel;
    this.storeManager = storeManager;
    this.keepRunning = keepRunning;
  }

  /**
   * Runs in a loop to dequeue new requests (blocking if there is not one),
   * sending the requests for processing to the
   * store manager, and enqueueing the responses from the store for handling
   * by the associated {@link ConnectionHandler}.
   */
  @Override
  public void run() {
    while (!Thread.currentThread().isInterrupted() && keepRunning.get()) {
      try {
        while (keepRunning.get()) {
          // blocks until a request is available to handle
          Request request = requestResponseChannel.dequeueRequest();
          StoreResponse resp = storeManager.handleCommand(request.getCommand());
          byte[] responseArray = Encoder.encodeResponse(request.getCommand(),
                  resp);
          if (responseArray != null) {
            requestResponseChannel.enqueueResponse(responseArray, request);
          }
        }
      } catch (InterruptedException e) {
        Thread.currentThread().interrupt();
      } catch (Exception e) {
        logger.log(Level.WARNING, e.getMessage());
      }
    }
  }
}
