package com.pnarayanan.memcached.server;

import com.pnarayanan.memcached.common.Command;

/**
 * The server deals with requests and responses. The Request class has an
 * associated {@link Command} with it, and also
 * carries the information about the associated {@link ChannelHandler} for
 * the {@link Command}.
 */
public class Request {
  private final Command command;
  private final ChannelHandler channelHandler;

  /**
   * Instantiate a Request object.
   *
   * @param command        the {@link Command} associated with this request.
   * @param channelHandler the {@link ChannelHandler} associated with this
   *                       request.
   */
  Request(Command command, ChannelHandler channelHandler) {
    this.command = command;
    this.channelHandler = channelHandler;
  }

  /**
   * @return the {@link Command} associated with this request.
   */
  public Command getCommand() {
    return command;
  }

  /**
   * @return the {@link ChannelHandler} associated with this request.
   */
  public ChannelHandler getChannelHandler() {
    return channelHandler;
  }
}
